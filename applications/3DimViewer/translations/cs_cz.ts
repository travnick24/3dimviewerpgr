<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ" sourcelanguage="en">
<context>
    <name>CCustomDockWidgetTitle</name>
    <message>
        <source>Save screenshot</source>
        <translation type="vanished">Uložit snímek obrazovky</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="114"/>
        <source>Adjusts VR density threshold</source>
        <translation>Nastaví práh denzit pro volume rendering</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="127"/>
        <location filename="../src/CCustomUI.cpp" line="141"/>
        <location filename="../src/CCustomUI.cpp" line="155"/>
        <source>Adjusts slice position</source>
        <translation>Upraví pozici řezu</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="351"/>
        <source>Copy Screenshot to Clipboard</source>
        <translation>Kopírovat Snímek Obrazovky do Schránky</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="352"/>
        <source>Save Screenshot</source>
        <translation>Uložit Snímek Obrazovky</translation>
    </message>
    <message>
        <location filename="../src/CCustomUI.cpp" line="356"/>
        <source>Save slice</source>
        <translation>Uložit řez</translation>
    </message>
</context>
<context>
    <name>CDataInfoDialog</name>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="20"/>
        <source>Data Information</source>
        <translation>Informace o datové sadě</translation>
    </message>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="51"/>
        <source>Parameter</source>
        <translation>Parametr</translation>
    </message>
    <message>
        <location filename="../ui/CDataInfoDialog.ui" line="56"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="234"/>
        <source>Patient Data</source>
        <translation>Pacient</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="235"/>
        <source>Dimensions</source>
        <translation>Rozměry</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="235"/>
        <source>%1 x %2 x %3 voxels</source>
        <translation>%1 x %2 x %3 voxelů</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="236"/>
        <source>Resolution</source>
        <translation>Rozlišení</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="236"/>
        <location filename="../src/CDataInfoDialog.cpp" line="237"/>
        <source>%1 x %2 x %3 mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="237"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="238"/>
        <source>Patient Name</source>
        <translation>Jméno pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="239"/>
        <source>Patient ID</source>
        <translation>ID pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="240"/>
        <source>Patient Position</source>
        <translation>Pozice pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="241"/>
        <source>Patient Birthday</source>
        <translation>Datum narození</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="242"/>
        <source>Patient Sex</source>
        <translation>Pohlaví</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="243"/>
        <source>Patient Description</source>
        <translation>Popis pacienta</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="244"/>
        <source>Study ID</source>
        <translation>ID studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="245"/>
        <source>Study Date</source>
        <translation>Datum studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="246"/>
        <source>Study Description</source>
        <translation>Popis studie</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="247"/>
        <source>Series ID</source>
        <translation>ID série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="248"/>
        <source>Series Date</source>
        <translation>Datum série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="249"/>
        <source>Series Time</source>
        <translation>Čas série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="250"/>
        <source>Series Description</source>
        <translation>Popis série</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="251"/>
        <source>Scan Options</source>
        <translation>Nastavení skenu</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="252"/>
        <source>Manufacturer</source>
        <translation>Výrobce</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="253"/>
        <source>Model Name</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="265"/>
        <source>Volume Transformation</source>
        <translation>Transformace dat</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="269"/>
        <location filename="../src/CDataInfoDialog.cpp" line="296"/>
        <source>Matrix</source>
        <translation>Matice</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="271"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="284"/>
        <source>Model %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="291"/>
        <source>Closed</source>
        <translation>Uzavřený</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="291"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="331"/>
        <source>Region %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="332"/>
        <source>Voxels</source>
        <translation>Počet voxelů</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="333"/>
        <source>Volume</source>
        <translation>Objem</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="351"/>
        <source>DICOM Info</source>
        <translation>Informace DICOM</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="426"/>
        <source>Copy</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <source>Bone Model</source>
        <translation type="obsolete">Model kosti</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="286"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="287"/>
        <source>Triangles</source>
        <translation>Trojúhelníků</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="288"/>
        <source>Nodes</source>
        <translation>Vrcholů</translation>
    </message>
    <message>
        <location filename="../src/CDataInfoDialog.cpp" line="289"/>
        <source>Components</source>
        <translation>Komponent</translation>
    </message>
</context>
<context>
    <name>CDicomTransferDialog</name>
    <message>
        <location filename="../ui/CDicomTransferDialog.ui" line="20"/>
        <source>DICOM Data Transfer</source>
        <translation>DICOM přenos dat</translation>
    </message>
    <message>
        <source>Destination path:</source>
        <translation type="vanished">Cílová cesta:</translation>
    </message>
    <message>
        <location filename="../ui/CDicomTransferDialog.ui" line="31"/>
        <source>Destination Path:</source>
        <translation>Cílová cesta:</translation>
    </message>
    <message>
        <location filename="../ui/CDicomTransferDialog.ui" line="95"/>
        <source>Info</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location filename="../ui/CDicomTransferDialog.ui" line="118"/>
        <source>Import DICOM Data...</source>
        <translation>Nahrát DICOM data...</translation>
    </message>
    <message>
        <location filename="../src/CDicomTransferDialog.cpp" line="1204"/>
        <source>Set DICOM data destination path</source>
        <translation>Nastavit cílovou cestu pro DICOM data</translation>
    </message>
</context>
<context>
    <name>CFilterDialog</name>
    <message>
        <location filename="../ui/CFilterDialog.ui" line="20"/>
        <source>Volume Filter</source>
        <translation>Filtrace objemových dat</translation>
    </message>
    <message>
        <location filename="../ui/CFilterDialog.ui" line="54"/>
        <source>Slice</source>
        <translation>Řez</translation>
    </message>
    <message>
        <location filename="../ui/CFilterDialog.ui" line="75"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/CFilterDialog.ui" line="81"/>
        <source>Strength</source>
        <translation>Síla filtru</translation>
    </message>
    <message>
        <location filename="../ui/CFilterDialog.ui" line="113"/>
        <source>%</source>
        <translation>%</translation>
    </message>
</context>
<context>
    <name>CInfoDialog</name>
    <message>
        <location filename="../ui/cinfodialog.ui" line="20"/>
        <source>Information</source>
        <translation>Informace</translation>
    </message>
    <message>
        <location filename="../ui/cinfodialog.ui" line="51"/>
        <source>Parameter</source>
        <translation>Parametr</translation>
    </message>
    <message>
        <location filename="../ui/cinfodialog.ui" line="56"/>
        <source>Value</source>
        <translation>Hodnota</translation>
    </message>
</context>
<context>
    <name>CModelsWidget</name>
    <message>
        <location filename="../src/modelswidget.cpp" line="207"/>
        <source>Click here to change model color</source>
        <translation>Klikněte sem pro změnu barvy modelu</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="215"/>
        <source>Remove model</source>
        <translation>Odstranit model</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="224"/>
        <source>Show information about model</source>
        <translation>Zobrazit informace o modelu</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="238"/>
        <source>Triangles: </source>
        <translation>Počet trojúhelníků: </translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="238"/>
        <source>, Nodes: </source>
        <translation>Počet vrcholů: </translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="238"/>
        <source>, Components: </source>
        <translation>Počet komponent: </translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="270"/>
        <source>Select color for model</source>
        <translation>Zvolte barvu modelu</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="294"/>
        <source>Do you really want to remove this model?</source>
        <translation>Opravdu chcete smazat tento model?</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="533"/>
        <source>Adjust Position...</source>
        <translation>Upravit pozici...</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="534"/>
        <source>Center Position</source>
        <translation>Umístit na střed</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="549"/>
        <source>Position</source>
        <translation>Pozice</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="552"/>
        <location filename="../src/modelswidget.cpp" line="553"/>
        <location filename="../src/modelswidget.cpp" line="554"/>
        <source> mm</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="568"/>
        <source>X:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="570"/>
        <source>Y:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="572"/>
        <source>Z:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="633"/>
        <source>Model Information</source>
        <translation>Informace o modelu</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="638"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="639"/>
        <source>Triangles</source>
        <translation>Počet trojúhelníků</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="640"/>
        <source>Nodes</source>
        <translation>Počet vrcholů</translation>
    </message>
    <message>
        <location filename="../src/modelswidget.cpp" line="641"/>
        <source>Components</source>
        <translation>Počet komponent</translation>
    </message>
</context>
<context>
    <name>CPluginInfoDialog</name>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="43"/>
        <source>OK</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="69"/>
        <source>Plugin Information</source>
        <translation>Informace o pluginech</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="90"/>
        <source>%1 found the following plugins in
%2</source>
        <translation>%1 našlo tyto pluginy na cestě
%2</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginInfoDialog.cpp" line="95"/>
        <source>%1 (Static Plugin)</source>
        <translation>%1 (Statický plugin)</translation>
    </message>
</context>
<context>
    <name>CPluginManager</name>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Pluginy</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="401"/>
        <source>&amp;Plugins</source>
        <translation>&amp;Pluginy</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="406"/>
        <source>Plugins Toolbar</source>
        <translation>Lišta s pluginy</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="494"/>
        <source>Show Help</source>
        <translation>Zobrazit nápovědu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="523"/>
        <source>Show/Hide Plugin Toolbar</source>
        <translation>Zobrazit/Skrýt lištu pluginu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="532"/>
        <source>Show/Hide Plugin Panel</source>
        <translation>Zobrazit/Skrýt panel pluginu</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="544"/>
        <source>Plugin Registration...</source>
        <translation>Registrace pluginu...</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="720"/>
        <source>A PDF viewer is required to view help!</source>
        <translation>K zobrazení nápovědy je potřeba PDF prohlížeč!</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="723"/>
        <source>Help file is missing!</source>
        <translation>Soubor s nápovědou nebyl nalezen!</translation>
    </message>
    <message>
        <location filename="../../../src/qtplugin/CPluginManager.cpp" line="729"/>
        <source>Couldn&apos;t open online help! Please verify your internet connection.</source>
        <translation>Nepodařilo se získat online nápovědu, zkontrolujte prosím své internetové připojení.</translation>
    </message>
</context>
<context>
    <name>CPreferencesDialog</name>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="26"/>
        <source>Preferences</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="48"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="55"/>
        <location filename="../ui/cpreferencesdialog.ui" line="58"/>
        <source>Selects language used in application.</source>
        <translation>Zvolí jazyk použitý v aplikaci.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="65"/>
        <source>Rendering Mode</source>
        <translation>Renderovací jádro</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="72"/>
        <location filename="../ui/cpreferencesdialog.ui" line="75"/>
        <source>Selects mode of rendering.</source>
        <translation>Zvolí režim vykreslování.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="79"/>
        <source>Single Threaded</source>
        <translation>Jedno vlákno</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="84"/>
        <source>Multi Threaded</source>
        <translation>Více vláken</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="92"/>
        <location filename="../ui/cpreferencesdialog.ui" line="95"/>
        <source>Allows logging.</source>
        <translation>Povolí vedení záznamu aplikace.</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="98"/>
        <source>Logging</source>
        <translation>Vedení záznamu aplikace</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="105"/>
        <location filename="../ui/cpreferencesdialog.ui" line="453"/>
        <source>Show Log...</source>
        <translation>Zobrazit záznam událostí...</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="115"/>
        <source>Link Model with Region by Default</source>
        <translation>Spojovat modely s regiony</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="181"/>
        <source>Last Used Path</source>
        <translation>Poslední použitá</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="188"/>
        <source>Current Project Path</source>
        <translation>Podle otevřeného projektu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="198"/>
        <source>Default Name of Saved Files</source>
        <translation>Výchozí název ukládaných souborů</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="204"/>
        <source>Patient Name</source>
        <translation>Jméno pacienta</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="211"/>
        <source>Project Folder Name</source>
        <translation>Jméno složky projektu</translation>
    </message>
    <message>
        <source>Show log...</source>
        <translation type="vanished">Zobrazit záznam událostí...</translation>
    </message>
    <message>
        <source>Link model with region by default</source>
        <translation type="vanished">Spojovat modely s regiony</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="172"/>
        <source>Save path for segmentation and model data</source>
        <translation>Nabízená cesta pro uložení segmentačních dat a modelu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="175"/>
        <source>Save Path</source>
        <translation>Nabízená cesta pro uložení</translation>
    </message>
    <message>
        <source>Last used path</source>
        <translation type="vanished">poslední použitá</translation>
    </message>
    <message>
        <source>Current project path</source>
        <translation type="vanished">podle otevřeného projektu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="122"/>
        <source>Background Color</source>
        <translation>Barva pozadí</translation>
    </message>
    <message>
        <source>Custom events</source>
        <translation type="vanished">Vlastní události</translation>
    </message>
    <message>
        <source>Event filter</source>
        <translation type="vanished">Záznam událostí</translation>
    </message>
    <message>
        <source>Models mirror region data</source>
        <translation type="vanished">Modely svázány s regiony</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="142"/>
        <source>DICOM Port</source>
        <translation>DICOM port</translation>
    </message>
    <message>
        <source>Default name of saved files</source>
        <translation type="vanished">Výchozí název ukládaných souborů</translation>
    </message>
    <message>
        <source>Patient name</source>
        <translation type="vanished">Jméno pacienta</translation>
    </message>
    <message>
        <source>Project folder name</source>
        <translation type="vanished">Jméno složky projektu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="229"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="234"/>
        <location filename="../ui/cpreferencesdialog.ui" line="242"/>
        <source>Shortcut</source>
        <translation>Klávesová zkratka</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="251"/>
        <source>Type to set shortcut</source>
        <translation>Zadejte klávesovou zkratku</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="258"/>
        <source>Set</source>
        <translation>Nastavit</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="265"/>
        <source>Clear</source>
        <translation>Smazat</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="286"/>
        <source>Path to Output Folder</source>
        <translation>Cesta k výstupní složce</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="330"/>
        <source>Event Types</source>
        <translation>Typy událostí</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="336"/>
        <source>Mouse Events</source>
        <translation>Události myši</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="343"/>
        <source>Keyboard Events</source>
        <translation>Události klávesnice</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="350"/>
        <source>Custom Events</source>
        <translation>Vlastní události</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="360"/>
        <source>Monitored Objects</source>
        <translation>Sledované objekty</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="366"/>
        <source>Push Buttons</source>
        <translation>Tlačítka</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="373"/>
        <source>Menu Items</source>
        <translation>Položky menu</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="380"/>
        <source>Spin Boxes</source>
        <translation>Přírůstková pole</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="387"/>
        <source>Check Boxes</source>
        <translation>Zaškrtávácí pole</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="394"/>
        <source>Radio Buttons</source>
        <translation>Přepínače</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="401"/>
        <source>Text Fields</source>
        <translation>Textová pole</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="408"/>
        <source>Combo Boxes</source>
        <translation>Kombinované seznamy</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="415"/>
        <source>Sliderds</source>
        <translation>Posuvníky</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="422"/>
        <source>OSG Windows</source>
        <translation>OSG okna</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="429"/>
        <source>Tab Bars</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="497"/>
        <location filename="../ui/cpreferencesdialog.ui" line="526"/>
        <source>General</source>
        <translation>Základní</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="531"/>
        <source>Shortcuts</source>
        <translation>Klávesové zkratky</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="536"/>
        <source>Event Filter</source>
        <translation>Záznam událostí</translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="52"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="186"/>
        <source>Select Directory</source>
        <translation>Zvolte složku</translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="399"/>
        <source>Alt+%1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/cpreferencesdialog.cpp" line="489"/>
        <source>Shortcut already used! Do you want to clear existing shortcut?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Event filtr</source>
        <translation type="vanished">Záznam událostí</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="279"/>
        <source>Enable</source>
        <translation>Povolit</translation>
    </message>
    <message>
        <source>Path to output folder</source>
        <translation type="vanished">Cesta k výstupní složce</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="308"/>
        <source>Browse</source>
        <translation>Procházet</translation>
    </message>
    <message>
        <source>Event types</source>
        <translation type="vanished">Typy událostí</translation>
    </message>
    <message>
        <source>Mouse events</source>
        <translation type="vanished">Události myši</translation>
    </message>
    <message>
        <source>Keyboard events</source>
        <translation type="vanished">Události klávesnice</translation>
    </message>
    <message>
        <source>Monitored objects</source>
        <translation type="vanished">Sledované objekty</translation>
    </message>
    <message>
        <source>Push buttons</source>
        <translation type="vanished">Tlačítka</translation>
    </message>
    <message>
        <source>Menu items</source>
        <translation type="vanished">Položky menu</translation>
    </message>
    <message>
        <source>Spin boxes</source>
        <translation type="vanished">Přírůstková pole</translation>
    </message>
    <message>
        <source>Check boxes</source>
        <translation type="vanished">Zaškrtávácí pole</translation>
    </message>
    <message>
        <source>Radio buttons</source>
        <translation type="vanished">Přepínače</translation>
    </message>
    <message>
        <source>Text fields</source>
        <translation type="vanished">Textová pole</translation>
    </message>
    <message>
        <source>Combo boxes</source>
        <translation type="vanished">Kombinované seznamy</translation>
    </message>
    <message>
        <source>Sliders</source>
        <translation type="vanished">Posuvníky</translation>
    </message>
    <message>
        <source>OSG windows</source>
        <translation type="vanished">OSG okna</translation>
    </message>
    <message>
        <source>Tab bars</source>
        <translation type="vanished">Záložky</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="436"/>
        <source>Lists</source>
        <translation>Seznamy</translation>
    </message>
    <message>
        <location filename="../ui/cpreferencesdialog.ui" line="443"/>
        <source>Tables</source>
        <translation>Tabulky</translation>
    </message>
</context>
<context>
    <name>CPreviewDialog</name>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="20"/>
        <source>Volume Filter</source>
        <translation>Filtrace objemových dat</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="62"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <source>Filter type</source>
        <translation type="vanished">Typ filtru</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="68"/>
        <source>Filter Type</source>
        <translation>Typ filtru</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="80"/>
        <source>Parameters</source>
        <translation>Parametry filtru</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="96"/>
        <source>Param1:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="128"/>
        <source>%</source>
        <translation>%</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="148"/>
        <source>Param2:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="185"/>
        <source>Param3:</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="228"/>
        <source>Original</source>
        <translation>Původní řez</translation>
    </message>
    <message>
        <location filename="../ui/CPreviewDialog.ui" line="268"/>
        <source>Filtered</source>
        <translation>Filtrovaný řez</translation>
    </message>
</context>
<context>
    <name>CSeriesSelectionDialog</name>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="20"/>
        <source>Please, select one of the found DICOM series...</source>
        <translation>Prosím, zvolte některou z nalezených datových sad...</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="29"/>
        <source>Sampling Factors</source>
        <translation>Převzorkování</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="86"/>
        <source>X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="96"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="106"/>
        <source>Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="149"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="154"/>
        <source>Series</source>
        <translation>Série</translation>
    </message>
    <message>
        <source>Patient  Name</source>
        <translation type="obsolete">Jméno pacienta</translation>
    </message>
    <message>
        <source>Series Date</source>
        <translation type="obsolete">Datum série</translation>
    </message>
    <message>
        <location filename="../ui/cseriesselectiondialog.ui" line="159"/>
        <source>Image Data</source>
        <translation>Obrazová data</translation>
    </message>
    <message>
        <location filename="../src/cseriesselectiondialog.cpp" line="280"/>
        <source>%1 x %2 x %3 voxels
%4 x %5 x %6 mm
%7 - %8</source>
        <translation>%1 x %2 x %3 voxelů
%4 x %5 x %6 mm
%7 - %8</translation>
    </message>
    <message>
        <location filename="../src/cseriesselectiondialog.cpp" line="410"/>
        <source>Please select a series.</source>
        <translation>Zvolte prosím sérii.</translation>
    </message>
</context>
<context>
    <name>CSeriesSelectionDialogPreview</name>
    <message>
        <location filename="../src/cseriesselectiondialog.cpp" line="497"/>
        <source>Preview</source>
        <translation>Náhled</translation>
    </message>
</context>
<context>
    <name>CThreadedDICOMReceiver</name>
    <message>
        <location filename="../src/CDicomTransferDialog.cpp" line="1008"/>
        <location filename="../src/CDicomTransferDialog.cpp" line="1011"/>
        <source>Cannot create network %1</source>
        <translation>Nelze vytvořit síť %1</translation>
    </message>
    <message>
        <location filename="../src/CDicomTransferDialog.cpp" line="1015"/>
        <source>Network initialized, port %1, ip %2</source>
        <translation>Síť inicializována, port %1, ip %2</translation>
    </message>
    <message>
        <location filename="../src/CDicomTransferDialog.cpp" line="1033"/>
        <location filename="../src/CDicomTransferDialog.cpp" line="1036"/>
        <source>Error %1</source>
        <translation>Chyba %1</translation>
    </message>
</context>
<context>
    <name>CVolumeLimiterDialog</name>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="20"/>
        <source>Please, select volume of your interest...</source>
        <translation>Prosím, omezte oblast zájmu pomocí žlutých značek...</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="37"/>
        <source>XY View</source>
        <translation>XY pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="56"/>
        <source>XZ View</source>
        <translation>XZ pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="75"/>
        <source>YZ View</source>
        <translation>YZ pohled</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="84"/>
        <source>Options</source>
        <translation>Volby</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="90"/>
        <source>Imaging Mode</source>
        <translation>Mód zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="97"/>
        <source>Mode of imaging</source>
        <translation>Režim zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="100"/>
        <source>Selects mode of imaging.</source>
        <translation>Zvolí režim zobrazování.</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="104"/>
        <source>RTG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="109"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="120"/>
        <source>Volume of Interest</source>
        <translation>Oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="128"/>
        <source>Min X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="138"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="158"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="178"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="202"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="222"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="242"/>
        <source>Volume of interest</source>
        <translation>Oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="148"/>
        <source>Min Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="168"/>
        <source>Min Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="192"/>
        <source>Max X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="212"/>
        <source>Max Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="232"/>
        <source>Max Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/cvolumelimiterdialog.ui" line="272"/>
        <location filename="../ui/cvolumelimiterdialog.ui" line="275"/>
        <source>Resets window zoom.</source>
        <translation>Obnoví původní přiblížení oken.</translation>
    </message>
</context>
<context>
    <name>CVolumeRenderingWidget</name>
    <message>
        <source>Soft tissues</source>
        <translation type="vanished">Měkké tkáně</translation>
    </message>
    <message>
        <source>Bone tissues</source>
        <translation type="vanished">Kostní tkáň</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="192"/>
        <source>Transparent</source>
        <translation>Průhledný</translation>
    </message>
    <message>
        <source>Air pockets</source>
        <translation type="vanished">Vzduchové kapsy</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="167"/>
        <source>Soft Tissues</source>
        <translation>Měkké tkáně</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="168"/>
        <source>Bone Tissues</source>
        <translation>Kostní tkáň</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="193"/>
        <source>Air Pockets</source>
        <translation>Vzduchové kapsy</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="194"/>
        <source>Bones (skull)</source>
        <translation>Kosti (lebka)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="195"/>
        <source>Bones (spine)</source>
        <translation>Kosti (páteř)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="196"/>
        <source>Bones (pelvis)</source>
        <translation>Kosti (pánev)</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="219"/>
        <location filename="../src/volumerenderingwidget.cpp" line="243"/>
        <source>Enhance Soft</source>
        <translation>Zvýraznit měkké tkáně</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="220"/>
        <location filename="../src/volumerenderingwidget.cpp" line="244"/>
        <source>Enhance Hard</source>
        <translation>Zvýraznit kosti</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="265"/>
        <source>Bone Surface</source>
        <translation>Povrch kosti</translation>
    </message>
    <message>
        <location filename="../src/volumerenderingwidget.cpp" line="266"/>
        <source>Skin Surface</source>
        <translation>Měkké tkáně</translation>
    </message>
    <message>
        <source>Enhance soft</source>
        <translation type="vanished">Zvýraznit měkké tkáně</translation>
    </message>
    <message>
        <source>Enhance hard</source>
        <translation type="vanished">Zvýraznit kosti</translation>
    </message>
    <message>
        <source>Bone surface</source>
        <translation type="vanished">Povrch kosti</translation>
    </message>
    <message>
        <source>Skin surface</source>
        <translation type="vanished">Měkké tkáně</translation>
    </message>
</context>
<context>
    <name>DensityWindowWidget</name>
    <message>
        <source>Density Window</source>
        <translation type="vanished">Densitní okénko</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="20"/>
        <source>Brightness / Contrast</source>
        <translation>Jas / Kontrast</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="56"/>
        <source>Current Settings</source>
        <translation>Aktuální nastavení</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="62"/>
        <location filename="../ui/densitywindowwidget.ui" line="65"/>
        <source>Adjusts width of brightness and contrast window.</source>
        <translation>Nastavení šířky okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="78"/>
        <location filename="../ui/densitywindowwidget.ui" line="81"/>
        <source>Adjusts center of brightness and contrast window.</source>
        <translation>Nastavení středu okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="97"/>
        <source>Center</source>
        <translation>Střed</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="155"/>
        <location filename="../ui/densitywindowwidget.ui" line="158"/>
        <source>Selects default setting of brightness and contrast.</source>
        <translation>Vybere výchozí nastavení jasu a kontrastu.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="168"/>
        <location filename="../ui/densitywindowwidget.ui" line="171"/>
        <source>Analyzes data and selects optimal setting of brightness and contrast.</source>
        <translation>Analyzuje data a vybere optimální nastavení jasu a kontrastu.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="181"/>
        <location filename="../ui/densitywindowwidget.ui" line="184"/>
        <source>Selects setting of brightness and contrast optimal for showing bones.</source>
        <translation>Vybere optimální nastavení jasu a kontrastu pro zobrazení kostí.</translation>
    </message>
    <message>
        <source>Adjusts center of density window.</source>
        <translation type="vanished">Upraví střed densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="104"/>
        <source>Width</source>
        <translation>Šířka</translation>
    </message>
    <message>
        <source>Adjusts width of density window.</source>
        <translation type="vanished">Upraví šířku densitního okénka</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="149"/>
        <source>Predefined Windows</source>
        <translation>Připravená okénka</translation>
    </message>
    <message>
        <source>Selects default setting of density window.</source>
        <translation type="vanished">Zvolí původní nastavení densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="161"/>
        <source>Default</source>
        <translation>Standardní</translation>
    </message>
    <message>
        <source>Analyzes data and selects optimal setting of density window.</source>
        <translation type="vanished">Analyzuje data a zvolí optimální nastavení densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="174"/>
        <source>Find Optimal</source>
        <translation>Nalézt optimální</translation>
    </message>
    <message>
        <source>Selects setting of density window optimal for showing bones.</source>
        <translation type="vanished">Zvolí nastavení optimální pro zobrazování kostí.</translation>
    </message>
    <message>
        <location filename="../ui/densitywindowwidget.ui" line="187"/>
        <source>Show Bones</source>
        <translation>Zvýraznit kosti</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Edit</source>
        <translation type="obsolete">Úpravy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="62"/>
        <source>Switch Mouse Mode</source>
        <translation>Přepnout mód myši</translation>
    </message>
    <message>
        <source>View</source>
        <translation type="vanished">Zobrazit</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="80"/>
        <source>Views</source>
        <translation>Pohledy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="90"/>
        <location filename="../src/mainwindow.cpp" line="4744"/>
        <source>Toolbars</source>
        <translation>Nástrojové lišty</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="100"/>
        <source>Control Panels</source>
        <translation>Kontrolní panely</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="115"/>
        <source>3D Scene</source>
        <translation>3D scéna</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="132"/>
        <source>Model</source>
        <translation>Model</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="140"/>
        <source>Data</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="211"/>
        <location filename="../ui/mainwindow.ui" line="530"/>
        <source>Main Toolbar</source>
        <translation>Hlavní lišta</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="234"/>
        <location filename="../ui/mainwindow.ui" line="544"/>
        <source>Views Toolbar</source>
        <translation>Lišta pohledů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="245"/>
        <location filename="../ui/mainwindow.ui" line="768"/>
        <source>Mouse Toolbar</source>
        <translation>Mód myši</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="256"/>
        <location filename="../ui/mainwindow.ui" line="782"/>
        <source>Visibility Toolbar</source>
        <translation>Viditelnost objektů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="267"/>
        <source>Application Toolbar</source>
        <translation>Aplikační lišta</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="291"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="309"/>
        <source>Ctrl+V</source>
        <translation></translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Soubor</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="14"/>
        <source>3DimViewer</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="124"/>
        <source>Layout</source>
        <translation>Uspořádání</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Nápověda</translation>
    </message>
    <message>
        <source>Filtering</source>
        <translation type="obsolete">Filtrování</translation>
    </message>
    <message>
        <source>Tools</source>
        <translation type="obsolete">Nástroje</translation>
    </message>
    <message>
        <source>Measurements</source>
        <translation type="obsolete">Měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="282"/>
        <source>Import Patient DICOM Data...</source>
        <translation>Importovat DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="285"/>
        <location filename="../ui/mainwindow.ui" line="288"/>
        <source>Loads DICOM dataset from a given directory.</source>
        <translation>Načte DICOM data ze zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="300"/>
        <source>Load Patient VLM Data...</source>
        <translation>Načíst objemová VLM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="303"/>
        <location filename="../ui/mainwindow.ui" line="306"/>
        <source>Loads density data from a specified VLM file.</source>
        <translation>Načte denzitní data ze zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="318"/>
        <source>Save Volumetric VLM Data...</source>
        <translation>Uložit objemová VLM data...</translation>
    </message>
    <message>
        <source>Saves volumetric data to a specified VLM file.</source>
        <translation type="obsolete">Uloží objemová data do zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="327"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="336"/>
        <source>Exit</source>
        <translation>Ukončit program</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="339"/>
        <location filename="../ui/mainwindow.ui" line="342"/>
        <source>Terminates the program.</source>
        <translation>Ukončí program.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="345"/>
        <source>Ctrl+X</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="354"/>
        <source>Print...</source>
        <translation>Tisk...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="357"/>
        <source>Print</source>
        <translation>Tisk</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="369"/>
        <source>Undo</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="372"/>
        <location filename="../ui/mainwindow.ui" line="375"/>
        <source>Undoes the last command.</source>
        <translation>Vrátí zpět poslední příkaz.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="378"/>
        <source>Ctrl+Z</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="387"/>
        <source>Redo</source>
        <translation>Opakovat</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="390"/>
        <location filename="../ui/mainwindow.ui" line="393"/>
        <source>Redoes the last command.</source>
        <translation>Obnoví poslední odvolaný příkaz.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="396"/>
        <source>Ctrl+Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="408"/>
        <location filename="../ui/mainwindow.ui" line="751"/>
        <source>3D View</source>
        <translation>3D scéna</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="411"/>
        <source>Shows/Hides the main 3D scene.</source>
        <translation>Zobrazí/Skryje hlavní 3D scénu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="426"/>
        <source>Axial View</source>
        <translation>Axiální pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="429"/>
        <location filename="../ui/mainwindow.ui" line="432"/>
        <source>Shows/Hides the axial (XY) plane view.</source>
        <translation>Zobrazí/Skryje pohled na axiální (XY) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="444"/>
        <source>Coronal View</source>
        <translation>Koronární pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="447"/>
        <location filename="../ui/mainwindow.ui" line="450"/>
        <source>Shows/Hides the coronal (XZ) plane view.</source>
        <translation>Zobrazí/Skryje pohled na koronární (XZ) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="462"/>
        <source>Sagittal View</source>
        <translation>Sagitální pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="465"/>
        <location filename="../ui/mainwindow.ui" line="468"/>
        <source>Shows/Hides the sagittal (YZ) plane view.</source>
        <translation>Zobrazí/Skryje pohled na sagitální (YZ) řez.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="483"/>
        <source>Panoramic View</source>
        <translation>Panoramatický pohled</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="498"/>
        <source>Normal Slice</source>
        <translation>Normálový řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="513"/>
        <source>Volume Rendering View</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="516"/>
        <location filename="../ui/mainwindow.ui" line="519"/>
        <source>Shows/Hides main volume rendering window.</source>
        <translation>Zobrazí/Skryje okno s volume renderingem.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="533"/>
        <source>Shows/Hides main toolbar.</source>
        <translation>Zobrazí/Skryje hlavní lištu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="547"/>
        <source>Shows/Hides views toolbar.</source>
        <translation>Zobrazí/Skryje lištu pohledů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1301"/>
        <source>Video Tutorials...</source>
        <translation>Video tutoriály...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1306"/>
        <location filename="../src/mainwindow.cpp" line="3507"/>
        <location filename="../src/mainwindow.cpp" line="5771"/>
        <source>Give Us Your Feedback...</source>
        <translation>Dejte nám zpětnou vazbu...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1311"/>
        <source>Tabify Panels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1314"/>
        <location filename="../ui/mainwindow.ui" line="1317"/>
        <source>Arrange all panels in tabs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Density Window Panel</source>
        <translation type="vanished">Panel densitního okénka</translation>
    </message>
    <message>
        <source>Shows/Hides the density window adjusting panel.</source>
        <translation type="vanished">Zobrazí/Skryje panel densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="568"/>
        <source>Ctrl+1</source>
        <translation>Ctrl+1</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="580"/>
        <source>Ortho Slices Panel</source>
        <translation>Panel kolmých řezů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="583"/>
        <location filename="../ui/mainwindow.ui" line="586"/>
        <source>Shows/Hides the ortho slices panel.</source>
        <translation>Zobrazí/Skryje panel kolmých řezů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="589"/>
        <source>Ctrl+2</source>
        <translation>Ctrl+2</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="601"/>
        <source>Trackball Mode</source>
        <translation>Manipulovat se scénou</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="604"/>
        <location filename="../ui/mainwindow.ui" line="607"/>
        <source>Switches mouse to scene manipulation mode.</source>
        <translation>Přepne myš do módu manipulace se scénou.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="619"/>
        <source>Object Manipulation</source>
        <translation>Manipulovat s objekty</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="622"/>
        <location filename="../ui/mainwindow.ui" line="625"/>
        <source>Switches mouse to the object manipulation mode so that you can change position of cross-sectional slices, implants, etc.</source>
        <translation>Přepne myš do módu manipulace s objekty.</translation>
    </message>
    <message>
        <source>Density Window Adjusting</source>
        <translation type="vanished">Měnit densitní okénko</translation>
    </message>
    <message>
        <source>Switches mouse to the density window adjusting mode. Press the left mouse button and modify the density window moving the mouse cursor.</source>
        <translation type="vanished">Přepne myš do módu nastavování densitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="648"/>
        <source>Create Surface Model</source>
        <translation>Vytvořit model povrchu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="651"/>
        <location filename="../ui/mainwindow.ui" line="654"/>
        <source>Creates polygonal surface model regarding the current tissue segmentation. For options see the tissue segmentation panel...</source>
        <translation>Vytvoří polygonální model povrchu segmentované tkáně...</translation>
    </message>
    <message>
        <source>Set Volume of Interest</source>
        <translation type="obsolete">Nastavit oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1239"/>
        <source>Clear Volume of Interest</source>
        <translation>Zrušit oblast zájmu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1250"/>
        <source>Regions Visible</source>
        <translation>Viditelnost regionů</translation>
    </message>
    <message>
        <source>Regions Contours Visible</source>
        <translation type="vanished">Viditelnost hranice regionů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1288"/>
        <source>Show Surface Model</source>
        <translation>Zobrazit model povrchu</translation>
    </message>
    <message>
        <source>Shows/Hides the surface model in the 3D view.</source>
        <translation type="vanished">Zobrazí/Skryje model povrchu v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="666"/>
        <source>Axial Slice</source>
        <translation>Axiální řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="669"/>
        <location filename="../ui/mainwindow.ui" line="672"/>
        <source>Shows/Hides the axial (XY) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje axiální (XY) řez v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="684"/>
        <source>Coronal Slice</source>
        <translation>Koronární řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="687"/>
        <location filename="../ui/mainwindow.ui" line="690"/>
        <source>Shows/Hides the coronal (XZ) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje koronoární (XZ) řez v 3D scéně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="702"/>
        <source>Sagittal Slice</source>
        <translation>Sagitální řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="705"/>
        <location filename="../ui/mainwindow.ui" line="708"/>
        <source>Shows/Hides the sagittal (YZ) slice in the 3D scene.</source>
        <translation>Zobrazí/Skryje sagitální (YZ) řez v 3D scéně.</translation>
    </message>
    <message>
        <source>Segmentation Panel</source>
        <translation type="vanished">Segmentační panel</translation>
    </message>
    <message>
        <source>Shows/Hides the tissue segmentation panel.</source>
        <translation type="vanished">Zobrazí/Skryje panel segmentace tkání.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="729"/>
        <source>Ctrl+4</source>
        <translation>Ctrl+4</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="737"/>
        <source>Tabs</source>
        <translation>Záložky</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="740"/>
        <location filename="../ui/mainwindow.ui" line="743"/>
        <source>Switches workspace to tabbed layout.</source>
        <translation>Umístí hlavní pohledy do záložek.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="754"/>
        <location filename="../ui/mainwindow.ui" line="757"/>
        <source>Switches workspace to a layout with the 3D scene as the main window.</source>
        <translation>Nastaví 3D scénu jako hlavní okno.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="771"/>
        <source>Shows/Hides mouse mode toolbar.</source>
        <translation>Zobrazí/Skryje lištu manipulace.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="785"/>
        <source>Shows/Hides slices visibility toolbar.</source>
        <translation>Zobrazí/Skryje lištu viditelnosti.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="797"/>
        <source>Volume Rendering Panel</source>
        <translation>Panel volume renderingu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="800"/>
        <location filename="../ui/mainwindow.ui" line="803"/>
        <source>Shows/Hides the volume rendering configuration panel.</source>
        <translation>Zobrazí/Skryje panel konfigurace volume renderingu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="806"/>
        <source>Ctrl+3</source>
        <translation>Ctrl+3</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="815"/>
        <source>Preferences...</source>
        <translation>Nastavení...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="818"/>
        <location filename="../ui/mainwindow.ui" line="821"/>
        <source>Shows application&apos;s options.</source>
        <translation>Zobrazí možnosti nastavení aplikace.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="830"/>
        <source>Show Data Properties...</source>
        <translation>Zobrazit informace o datové sadě...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="833"/>
        <location filename="../ui/mainwindow.ui" line="836"/>
        <source>Shows information about the loaded dataset.</source>
        <translation>Zobrazí informace o aktuální datové sadě.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="845"/>
        <source>Load STL Model...</source>
        <translation>Načíst STL model...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="848"/>
        <location filename="../ui/mainwindow.ui" line="851"/>
        <source>Loads surface model from a specified STL file.</source>
        <translation>Načte povrchový (polygonální) model ze zadaného souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="856"/>
        <source>About...</source>
        <translation>O aplikaci...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="859"/>
        <location filename="../ui/mainwindow.ui" line="862"/>
        <source>Shows basic information about the program.</source>
        <translation>Zobrazí základní informace o aplikaci.</translation>
    </message>
    <message>
        <source>About Plugins...</source>
        <translation type="vanished">O pluginech...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="870"/>
        <location filename="../ui/mainwindow.ui" line="873"/>
        <source>Shows basic information about loaded plugins.</source>
        <translation>Zobrazí základní informace o načtených pluginech.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1109"/>
        <source>Save / Anonymize DICOM Series...</source>
        <translation>Uložit / Anonymizovat DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1189"/>
        <source>Close Active Panel</source>
        <translation>Zavřít aktivní panel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1192"/>
        <source>Ctrl+F4</source>
        <translation>Ctrl+F4</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1197"/>
        <source>Previous Panel</source>
        <translation>Předchozí panel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1200"/>
        <source>Ctrl+Shift+Tab</source>
        <translation>Ctrl+Shift+Tab</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1205"/>
        <source>Next Panel</source>
        <translation>Následující panel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1208"/>
        <source>Ctrl+Tab</source>
        <translation>Ctrl+Tab</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1213"/>
        <source>Volume Filter...</source>
        <translation>Filtr objemových dat...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1218"/>
        <source>Sharpening Filter...</source>
        <translation>Zaostřovací filtr...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1221"/>
        <location filename="../ui/mainwindow.ui" line="1224"/>
        <source>Applies sharpening filter on the volumetric data.</source>
        <translation>Aplikuje zaostřovací filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1229"/>
        <source>Receive DICOM Data...</source>
        <translation>Příjmout DICOM data...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3650"/>
        <source>Gaussian Filter</source>
        <translation>Gaussův filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="881"/>
        <location filename="../ui/mainwindow.ui" line="884"/>
        <source>Applies 3D Gaussian filter on the volumetric data.</source>
        <translation>Aplikuje 3D Gaussův filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3840"/>
        <source>Median Filter</source>
        <translation>Mediánový filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="892"/>
        <location filename="../ui/mainwindow.ui" line="895"/>
        <source>Applies median filter on the volumetric data.</source>
        <translation>Aplikuje mediánový filtr na objemová data.</translation>
    </message>
    <message>
        <source>3D Anisotropic Filter</source>
        <translation type="vanished">3D anizotropní filtr</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="903"/>
        <location filename="../ui/mainwindow.ui" line="906"/>
        <source>Applies 3D anisotropic filter on the volumetric data.</source>
        <translation>Aplikuje 3D anizotropní filtr na objemová data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="914"/>
        <source>Grid</source>
        <translation>Mříž</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="917"/>
        <location filename="../ui/mainwindow.ui" line="920"/>
        <source>Switches workspace to a grid layout with cross-sectional slices and 3D scene.</source>
        <translation>Nastaví do hlavního okna mříž hlavních pohledů na scénu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="931"/>
        <source>Show Information Widgets</source>
        <translation>Zobrazit základní informace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="934"/>
        <location filename="../ui/mainwindow.ui" line="937"/>
        <source>Shows/Hides special information widgets in all views.</source>
        <translation>Zobrazí/Skryje základní informace ve všech pohledech.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="956"/>
        <location filename="../ui/mainwindow.ui" line="959"/>
        <source>Shows Help.</source>
        <translation>Zobrazí Nápovědu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="977"/>
        <location filename="../ui/mainwindow.ui" line="980"/>
        <source>Switches mouse to scene scale so that you can scale scene to fit into window.</source>
        <translation>Přepne myš do módu škálování scény.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1021"/>
        <location filename="../ui/mainwindow.ui" line="1024"/>
        <source>Saves generated model to a specified STL file.</source>
        <translation>Uloží vytvořený model do zadaného STL souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1032"/>
        <location filename="../ui/mainwindow.ui" line="1035"/>
        <source>Sends volumetric data via DataExpress Service.</source>
        <translation>Odešle objemová data skrz DataExpress.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1047"/>
        <source>Measure Density Value [Hu]</source>
        <translation>Měřit hustotu [Hu]</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1050"/>
        <location filename="../ui/mainwindow.ui" line="1053"/>
        <source>To measure local density, specify a point using the mouse cursor and click the left button.</source>
        <translation>Pro změření hustoty zadejte bod pomocí kurzoru myši a kliknutím levého tlačitka.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1065"/>
        <source>Measure Distance [mm]</source>
        <translation>Měřit vzdálenost [mm]</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1068"/>
        <location filename="../ui/mainwindow.ui" line="1071"/>
        <source>Measure distance by clicking the left mouse button and dragging.</source>
        <translation>Změřte vzdálenost kliknutím levého tlačítka myši a tažením.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1080"/>
        <source>Clear Measurements</source>
        <translation>Vymazat výsledky měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1083"/>
        <location filename="../ui/mainwindow.ui" line="1086"/>
        <source>Clears all visible measurement results.</source>
        <translation>Smaže všechny viditelné výsledky měření.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1101"/>
        <location filename="../ui/mainwindow.ui" line="1104"/>
        <source>Shows/hides volume rendering in the 3D scene.</source>
        <translation>Zobrazí/Skryje volume rendering v 3D scéně.</translation>
    </message>
    <message>
        <source>Save DICOM Series...</source>
        <translation type="vanished">Uložit DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1112"/>
        <location filename="../ui/mainwindow.ui" line="1115"/>
        <source>Saves DICOM Series to a given directory.</source>
        <translation>Uloží DICOM sérii do zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1124"/>
        <source>Import DICOM Data from ZIP...</source>
        <translation>Importovat DICOM data ze ZIP archivu...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1136"/>
        <location filename="../src/mainwindow.cpp" line="1134"/>
        <source>Models List</source>
        <translation>Seznam modelů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1139"/>
        <location filename="../ui/mainwindow.ui" line="1142"/>
        <source>Shows/Hides the models list.</source>
        <translation>Zobrazí/Skryje seznam modelů.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1145"/>
        <source>Ctrl+5</source>
        <translation>Ctrl+5</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1153"/>
        <source>Equalize</source>
        <translation>Ekvalizace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1156"/>
        <location filename="../ui/mainwindow.ui" line="1159"/>
        <location filename="../ui/mainwindow.ui" line="1170"/>
        <location filename="../ui/mainwindow.ui" line="1173"/>
        <source>Does not change the original data.</source>
        <translation>Nemění originální data.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1167"/>
        <source>Sharpen</source>
        <translation>Doostření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1178"/>
        <source>Save Original DICOM Series...</source>
        <translation>Uložit originální DICOM data...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1181"/>
        <location filename="../ui/mainwindow.ui" line="1184"/>
        <source>Saves original DICOM Series to a given directory.</source>
        <translation>Uloží původní data série DICOM do zadaného adresáře.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1234"/>
        <source>Set Volume of Interest...</source>
        <translation>Nastavit oblast zájmu...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1261"/>
        <source>Region Contours Visible</source>
        <translation>Viditelnost hranice regionů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1296"/>
        <source>Volume of Interest Visible</source>
        <translation>Viditelnost oblasti zájmu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="223"/>
        <location filename="../ui/mainwindow.ui" line="948"/>
        <source>Panels Toolbar</source>
        <translation>Lišta panelů</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="31"/>
        <source>&amp;File</source>
        <translation>&amp;Soubor</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="35"/>
        <source>Recent</source>
        <translation>Naposledy otevřené</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="58"/>
        <source>&amp;Edit</source>
        <translation>Úp&amp;ravy</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="76"/>
        <source>&amp;View</source>
        <translation>&amp;Zobrazit</translation>
    </message>
    <message>
        <source>&amp;Model</source>
        <translation type="vanished">M&amp;odel</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="166"/>
        <source>&amp;Help</source>
        <translation>&amp;Nápověda</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="177"/>
        <source>Fil&amp;tering</source>
        <translation>&amp;Filtrování</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="186"/>
        <source>Me&amp;asurements</source>
        <translation>&amp;Měření</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="195"/>
        <source>Segmentation</source>
        <translation>Segmentace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="321"/>
        <location filename="../ui/mainwindow.ui" line="324"/>
        <source>Saves density data to a specified VLM file.</source>
        <translation>Uloží denzitní data do zadaného VLM souboru.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="559"/>
        <source>Brightness / Contrast Panel</source>
        <translation>Jas / Kontrast</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="562"/>
        <location filename="../ui/mainwindow.ui" line="565"/>
        <source>Shows/Hides the brightness and contrast adjusting panel.</source>
        <translation>Zobrazí/Skryje panel pro úpravu jasu a kontrastu.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="637"/>
        <source>Brightness / Contrast Adjusting</source>
        <translation>Nastavení jasu a kontrastu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="640"/>
        <location filename="../ui/mainwindow.ui" line="643"/>
        <source>Switches mouse to the brightness and contrast adjusting mode. Press the left mouse button and modify the brightness and contrast by moving the mouse cursor.</source>
        <translation>Přepne myš do módu pro úpravu jasu a kontrastu. Držte levé tlačítko myši a pohybem kurzoru upravujte jas a kontrast.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="720"/>
        <source>Quick Tissue Model Creation Panel</source>
        <translation>Rychlé vytvoření modelu tkáně</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="723"/>
        <location filename="../ui/mainwindow.ui" line="726"/>
        <source>Shows/Hides the quick tissue model creation panel.</source>
        <translation>Zobrazí/Skryje panel pro rychlé vytvoření modelu tkáně.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="867"/>
        <source>Plugins List...</source>
        <translation>Seznam pluginů...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="878"/>
        <source>Gaussian Filter...</source>
        <translation>Gaussův filtr...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="889"/>
        <source>Median Filter...</source>
        <translation>Mediánový filtr...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="900"/>
        <source>3D Anisotropic Filter...</source>
        <translation>3D Anizotropní filtr...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="953"/>
        <source>Show Help</source>
        <translation>Zobrazit nápovědu</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="962"/>
        <source>F1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="974"/>
        <source>Scale Scene</source>
        <translation>Měřítko scény</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="985"/>
        <source>Load User Perspective</source>
        <translation>Načíst rozložení oken</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="988"/>
        <source>Loads the user defined windows layout.</source>
        <translation>Načte uživatelské rozložení oken (panelů, pohledů, lišt, apod.).</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="996"/>
        <source>Save User Perspective</source>
        <translation>Uložit rozložení oken</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="999"/>
        <source>Saves the current windows layout.</source>
        <translation>Uloží aktuální rozložení oken (panelů, pohledů, lišt, apod.).</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1007"/>
        <source>Load Default Perspective</source>
        <translation>Načíst standardní rozložení</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1010"/>
        <location filename="../ui/mainwindow.ui" line="1013"/>
        <source>Loads the default windows layout.</source>
        <translation>Načte výchozí rozložení oken.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1018"/>
        <source>Save STL Model...</source>
        <translation>Uložit STL model...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1029"/>
        <source>Send Data...  (DataExpress Service)</source>
        <translation>Odeslat data... (služba DataExpress)</translation>
    </message>
    <message>
        <source>Axial / XY Slice</source>
        <translation type="obsolete">Axiální / XY řez</translation>
    </message>
    <message>
        <source>Coronal / XZ Slice</source>
        <translation type="obsolete">Koronární / XZ řez</translation>
    </message>
    <message>
        <source>Sagittal / YZ Slice</source>
        <translation type="obsolete">Sagitální / YZ řez</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1098"/>
        <location filename="../src/mainwindow.cpp" line="1123"/>
        <source>Volume Rendering</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <source>Process model</source>
        <translation type="vanished">Zpracovat model</translation>
    </message>
    <message>
        <source>Visualization</source>
        <translation type="vanished">Vizualizace</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1266"/>
        <source>Smooth</source>
        <translation>Hladké stínování</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1271"/>
        <source>Flat</source>
        <translation>Ploché stínování</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.ui" line="1276"/>
        <source>Wire</source>
        <translation>Drátový model</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="848"/>
        <source>Change panel visibility</source>
        <translation>Viditelnost panelů</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="855"/>
        <source>Fullscreen</source>
        <translation>Celá obrazovka</translation>
    </message>
    <message>
        <source>Density Window</source>
        <translation type="vanished">Densitní okénko</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Ortho Slices</source>
        <translation>Kolmé řezy</translation>
    </message>
    <message>
        <source>Tissue Segmentation</source>
        <translation type="vanished">Segmentace tkání</translation>
    </message>
    <message>
        <source>Models list</source>
        <translation type="vanished">Seznam modelů</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <source>You have an unsaved segmentation data! All unsaved changes will be lost. Are your sure?</source>
        <translation>Segmentační data byla změněna, ale ne uložena. Při ukončení programu budou neuložené změny ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <source>All unsaved changes will be lost. Are your sure?</source>
        <translation>Všechny neuložené změny budou při ukončení programu ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1476"/>
        <location filename="../src/mainwindow.cpp" line="1499"/>
        <source>Load DICOM Data</source>
        <translation>Prosím, zadejte adresář obsahující vstupní DICOM data...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1499"/>
        <source>ZIP archive (*.zip)</source>
        <translation>ZIP archiv (*.zip)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1564"/>
        <source>Scanning the directory for DICOM datasets, please wait...</source>
        <translation>Vyhledávání DICOM datových sad, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1581"/>
        <source>No valid DICOM datasets have been found in a given directory!</source>
        <translation>V zadaném adresáři nebyla nalaezena žádná DICOM data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1614"/>
        <source>Loading input DICOM dataset, please wait...</source>
        <translation>Načítání DICOM datové sady, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1623"/>
        <source>Failed to load the DICOM dataset!</source>
        <translation>Nepodařilo se načíst model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1680"/>
        <source>You have an unsaved data! All unsaved changes will be lost. Do you want to continue?</source>
        <translation>Všechny neuložené změny budou při ukončení programu ztraceny. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1763"/>
        <source>Please, choose an input volume data to open...</source>
        <translation>Prosím, vyberte vstupní data...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1763"/>
        <location filename="../src/mainwindow.cpp" line="1912"/>
        <source>Volume Data (*.vlm)</source>
        <translation>Objemová data (*.vlm)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1805"/>
        <source>Loading input volumetric data, please wait...</source>
        <translation>Načítám objemová data, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1895"/>
        <source>Failed to read input volumetric data!</source>
        <translation>Nepodařilo se načíst vstupní objemová data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1912"/>
        <source>Choose an output file...</source>
        <translation>Zadejte výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1921"/>
        <source>Saving volumetric data, please wait...</source>
        <translation>Ukládám objemová data, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1966"/>
        <source>Failed to save volumetric data!</source>
        <translation>Nepodařilo se uložit objemová data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1988"/>
        <location filename="../src/mainwindow.cpp" line="2151"/>
        <source>Please, choose a directory where to save current DICOM dataset...</source>
        <translation>Zvolte cílový adresář pro uložení DICOM dat....</translation>
    </message>
    <message>
        <source>Failed to copy file %1. Retry?</source>
        <translation type="vanished">Při kopírování souboru %1 došlo k chybě. Opakovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2084"/>
        <source>Save All Data</source>
        <translation>Uložit aktuální data</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2087"/>
        <source>Save All Segmented Areas</source>
        <translation>Uložit všechny osegmentované oblasti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2089"/>
        <source>Save Active Region Only</source>
        <translation>Uložit pouze aktivní region</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2096"/>
        <source>Save Data in Volume of Interest</source>
        <translation>Uložit data v oblasti zájmu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4574"/>
        <source>Intensity Level: %1</source>
        <translation type="unfinished">Intenzita: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="5776"/>
        <source>We are surveying users about their experience and satisfaction with 3DimViewer. We appreciate your response. Thank you.</source>
        <translation>Zajímá nás spokojenost uživatelů aplikace 3DimViewer. Ceníme si Vaší odpovědi. Děkujeme.</translation>
    </message>
    <message>
        <source>Save DICOM</source>
        <translation type="vanished">Uložení DICOM</translation>
    </message>
    <message>
        <source>Save all data</source>
        <translation type="vanished">Uložit aktuální data</translation>
    </message>
    <message>
        <source>Save all segmented areas</source>
        <translation type="vanished">Uložit všechny osegmentované oblasti</translation>
    </message>
    <message>
        <source>Save active region only</source>
        <translation type="vanished">Uložit pouze aktivní region</translation>
    </message>
    <message>
        <source>Save data in volume of interest</source>
        <translation type="vanished">Uložit data v oblasti zájmu</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2101"/>
        <source>DICOM Properties</source>
        <translation>DICOM vlastnosti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2104"/>
        <source>Compress</source>
        <translation>Komprimovat</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2109"/>
        <source>Anonymize As</source>
        <translation>Anonymizovat jako</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2114"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2116"/>
        <source>Anonymous</source>
        <translation>Anonymní</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2119"/>
        <source>Id:</source>
        <translation></translation>
    </message>
    <message>
        <source>Segmented areas can&apos;t be exported with the current product license.</source>
        <translation type="vanished">Aktuální licence neumožňuje export segmentovaných oblastí. </translation>
    </message>
    <message>
        <source>Save segmented areas only</source>
        <translation type="obsolete">Uložit pouze osegmentované oblasti</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2166"/>
        <source>Specified directory already contains DICOM files! Do you really want to continue?</source>
        <translation>Zvolený adresář obsahuje DICOM soubory! Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2199"/>
        <source>Saving DICOM data, please wait...</source>
        <translation>Ukládájí se DICOM data, čekejte prosím...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2272"/>
        <source>Choose an input binary STL model to load...</source>
        <translation>Vyberte vstupní STL model pro načtení...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2272"/>
        <source>Stereo litography model (*.stl);;Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3576"/>
        <source>A PDF viewer is required to view help!</source>
        <translation>K zobrazení nápovědy je potřeba PDF prohlížeč!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3579"/>
        <source>Help file is missing!</source>
        <translation>Soubor s nápovědou nebyl nalezen!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4965"/>
        <location filename="../src/mainwindow.cpp" line="4966"/>
        <location filename="../src/mainwindow.cpp" line="4967"/>
        <source>Process using %1</source>
        <translation>Zpracovat pomocí %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4971"/>
        <source>There are none known tools available!</source>
        <translation>Nejsou k dispozici žádné nástroje pro zpracování!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="5148"/>
        <source>The maximum number of loadable models reached!</source>
        <translation>Byl dosažen maximální počet podporovaných modelů!</translation>
    </message>
    <message>
        <source>STL Data (*.stl)</source>
        <translation type="obsolete">STL data (*.stl)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2561"/>
        <source>Failed to load binary STL model!</source>
        <translation>Nepodařilo se načíst vstupní STL model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2632"/>
        <location filename="../src/mainwindow.cpp" line="4923"/>
        <source>No model selected!</source>
        <translation>Není zvolen žádný model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2644"/>
        <location filename="../src/mainwindow.cpp" line="4932"/>
        <source>No STL data!</source>
        <translation>Nejsou k dispozici žádná STL data!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2664"/>
        <source>Data can&apos;t be exported with the current product license.</source>
        <translation>S aktuální licencí není možné exportovat zvolená data.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2676"/>
        <source>Please, specify an output file...</source>
        <translation>Zadejte výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2676"/>
        <source>Stereo litography model (*.stl);;Binary Polygon file format (*.ply);;ASCII Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2695"/>
        <source>ASCII Polygon file format (*.ply)</source>
        <translation></translation>
    </message>
    <message>
        <source>Loaded volume is cropped. It is recommended to include crop information in the exported STL unless you plan to load the model in VLM data.</source>
        <translation type="obsolete">Načtená data jsou oříznutá. Pokud neexportujete STL model pro využití s VLM daty, je vhodné exportovat model s informacemi o ořezu dat.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2743"/>
        <source>Failed to save binary STL model!</source>
        <translation>Nepodařilo se uložit binární STL model!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3036"/>
        <source>VR Error!</source>
        <translation>Chyba inicializace Volume Renderingu!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3164"/>
        <source>3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3172"/>
        <source>Axial / XY</source>
        <translation>Axiální / XY</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3180"/>
        <source>Coronal / XZ</source>
        <translation>Koronární / XZ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3188"/>
        <source>Sagittal / YZ</source>
        <translation>Sagitální / YZ</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3341"/>
        <source>Creating surface model, please wait...</source>
        <translation>Vytvářím povrchový model, prosím čekejte...</translation>
    </message>
    <message>
        <source>Reducing flat areas, please wait...</source>
        <translation type="obsolete">Redukuji rovné plochy, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3380"/>
        <source>Reducing small areas, please wait...</source>
        <translation>Redukuji příliš malé oblaste, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3385"/>
        <source>Smoothing model, please wait...</source>
        <translation>Vyhlazuji model, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3398"/>
        <source>Decimating model, please wait...</source>
        <translation>Probíhá decimace modelu, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3483"/>
        <source>You must restart the application to apply the changes.</source>
        <translation>Nyní je nutné restartovat aplikaci.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3654"/>
        <location filename="../src/mainwindow.cpp" line="3757"/>
        <location filename="../src/mainwindow.cpp" line="3844"/>
        <location filename="../src/mainwindow.cpp" line="3931"/>
        <source>By applying the filter you will modify and ovewrite the original density data! Do you want to proceed?</source>
        <translation>Aplikací filtru modifikujete originální data! Chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3753"/>
        <source>Anisotropic Filter</source>
        <translation>Anizotropní filtr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3927"/>
        <source>Sharpen Filter</source>
        <translation>Zaostřovací filtr</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4130"/>
        <source>Not implemented</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4334"/>
        <source>ONLY for internal testing and not for distribution!</source>
        <translation>Pouze pro interní testování, nešířit!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4336"/>
        <source>ONLY for testing!</source>
        <translation>Pouze pro testování!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4472"/>
        <location filename="../src/mainwindow.cpp" line="4556"/>
        <source>Please specify an output file...</source>
        <translation>Zadejte prosím výstupní soubor...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4472"/>
        <location filename="../src/mainwindow.cpp" line="4556"/>
        <source>JPEG Image (*.jpg);;PNG Image (*.png);;BMP Image (*.bmp)</source>
        <translation>Obrázek JPEG (*.jpg);;Obrázek PNG (*.png);;Obrázek BMP (*.bmp)</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4479"/>
        <source>Save failed! Please check available diskspace and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4574"/>
        <source>Density Level: %1</source>
        <translation>Hustota: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="847"/>
        <location filename="../src/mainwindow.cpp" line="4727"/>
        <source>Panels</source>
        <translation>Panely</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1095"/>
        <source>Brightness / Contrast</source>
        <translation>Jas / Kontrast</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Quick Tissue Model Creation</source>
        <translation>Rychlé vytvoření modelu tkáně</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2031"/>
        <source>Failed to copy file %1. Make sure, that the destination directory doesn&apos;t include a file with the same name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2069"/>
        <source>Save / Anonymize DICOM</source>
        <translation>Uložit / Anonymizovat DICOM</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="2076"/>
        <source>Image Data Manipulation</source>
        <translation>Manipulace s obrazovými daty</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="4752"/>
        <source>Mouse Mode</source>
        <translation>Mód myši</translation>
    </message>
    <message>
        <source>Dimensions: 	%1 x %2 x %3 voxels
Resolution: 	%4 x %5 x %6 mm
Patient Name: 	%7
Patient ID: 	%8
Series Date: 	%9</source>
        <translation type="obsolete">Rozměry: 	%1 x %2 x %3 voxels
Rozlišení: 	%4 x %5 x %6 mm
Jméno pacienta:	%7
ID pacienta: 	%8
Datum série: 	%9</translation>
    </message>
    <message>
        <source>Information about the dataset...</source>
        <translation type="obsolete">Informace o aktuální datové sadě...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3512"/>
        <source>About </source>
        <translation>O aplikaci </translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3514"/>
        <source>Lightweight 3D DICOM viewer.

</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3516"/>
        <source>http://www.3dim-laboratory.cz/</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3518"/>
        <source>Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3659"/>
        <location filename="../src/mainwindow.cpp" line="3765"/>
        <location filename="../src/mainwindow.cpp" line="3849"/>
        <location filename="../src/mainwindow.cpp" line="3936"/>
        <source>Filtering volumetric data, please wait...</source>
        <translation>Probíhá filtrování objemových dat, prosím čekejte...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3686"/>
        <location filename="../src/mainwindow.cpp" line="3788"/>
        <location filename="../src/mainwindow.cpp" line="3873"/>
        <location filename="../src/mainwindow.cpp" line="3985"/>
        <source>Filtering aborted!</source>
        <translation>Filtrování selhalo!</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Pluginy</translation>
    </message>
    <message>
        <source>Show/Hide Plugin Toolbar</source>
        <translation type="obsolete">Zobrazit/Skrýt lištu pluginu</translation>
    </message>
    <message>
        <source>Show/Hide Plugin Panel</source>
        <translation type="obsolete">Zobrazit/Skrýt panel pluginu</translation>
    </message>
</context>
<context>
    <name>ModelsWidget</name>
    <message>
        <location filename="../ui/modelswidget.ui" line="14"/>
        <source>Models</source>
        <translation>Modely</translation>
    </message>
    <message>
        <source>Buy Segmentation Plugins</source>
        <translation type="vanished">Zakoupit segmentační pluginy</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="43"/>
        <source>Name</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="48"/>
        <source>Visible</source>
        <translation>Viditelnost</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="53"/>
        <source>Cut</source>
        <translation>2D Řez</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="71"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="83"/>
        <source>Use DICOM Coordinates</source>
        <translation>Zohlednit DICOM souřadnice</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="90"/>
        <source>Save Selected Model</source>
        <translation>Uložit vybraný model</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="38"/>
        <source>Color</source>
        <translation>Barva</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="20"/>
        <source>Try Advanced Segmentation Plugins</source>
        <translation>Vyzkoušet pokročilé segmentační pluginy</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="58"/>
        <source>Remove</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <source>Load/Save</source>
        <translation type="vanished">Načtení/Uložení</translation>
    </message>
    <message>
        <location filename="../ui/modelswidget.ui" line="77"/>
        <location filename="../ui/modelswidget.ui" line="80"/>
        <source>Transform according to original DICOM coordinates for later use in other software.</source>
        <translation>Transformovat na základě DICOM souřadnic pro pozdější načtení v jiném software.</translation>
    </message>
    <message>
        <source>Use DICOM coordinates</source>
        <translation type="vanished">Zohlednit DICOM souřadnice</translation>
    </message>
    <message>
        <source>Include information on volume transformation which was applied on data load.</source>
        <translation type="obsolete">Zohlednit transformaci objemových dat aplikovanou při jejich načtení.</translation>
    </message>
    <message>
        <source>Use Viewer transformation (VLM incompatible)</source>
        <translation type="obsolete">Zohlednit transformaci při načtení (nekompatibilní s VLM)</translation>
    </message>
</context>
<context>
    <name>OrthoSlicesWidget</name>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="26"/>
        <source>Ortho Slices</source>
        <translation>Kolmé řezy</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="53"/>
        <source>Axial / XY Slice</source>
        <translation>Axiální / XY řez</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="59"/>
        <location filename="../ui/orthosliceswidget.ui" line="62"/>
        <location filename="../ui/orthosliceswidget.ui" line="84"/>
        <location filename="../ui/orthosliceswidget.ui" line="87"/>
        <location filename="../ui/orthosliceswidget.ui" line="131"/>
        <location filename="../ui/orthosliceswidget.ui" line="134"/>
        <location filename="../ui/orthosliceswidget.ui" line="153"/>
        <location filename="../ui/orthosliceswidget.ui" line="156"/>
        <location filename="../ui/orthosliceswidget.ui" line="197"/>
        <location filename="../ui/orthosliceswidget.ui" line="200"/>
        <location filename="../ui/orthosliceswidget.ui" line="219"/>
        <location filename="../ui/orthosliceswidget.ui" line="222"/>
        <source>Adjusts slice position.</source>
        <translation>Upraví pozici řezu.</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="97"/>
        <location filename="../ui/orthosliceswidget.ui" line="100"/>
        <location filename="../ui/orthosliceswidget.ui" line="163"/>
        <location filename="../ui/orthosliceswidget.ui" line="166"/>
        <location filename="../ui/orthosliceswidget.ui" line="232"/>
        <location filename="../ui/orthosliceswidget.ui" line="235"/>
        <source>Selects displaying mode of slice.</source>
        <translation>Zvolí režim zobrazení řezu.</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="104"/>
        <location filename="../ui/orthosliceswidget.ui" line="170"/>
        <location filename="../ui/orthosliceswidget.ui" line="239"/>
        <source>Default</source>
        <translation>Standardní</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="109"/>
        <location filename="../ui/orthosliceswidget.ui" line="175"/>
        <location filename="../ui/orthosliceswidget.ui" line="244"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="114"/>
        <location filename="../ui/orthosliceswidget.ui" line="180"/>
        <location filename="../ui/orthosliceswidget.ui" line="249"/>
        <source>RTG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="125"/>
        <source>Coronal / XZ Slice</source>
        <translation>Koronární / XZ řez</translation>
    </message>
    <message>
        <location filename="../ui/orthosliceswidget.ui" line="191"/>
        <source>Sagittal / YZ Slice</source>
        <translation>Sagitální / YZ řez</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="79"/>
        <source>Invalid parameter detected in C runtime function</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="412"/>
        <source>Your hardware doesn&apos;t meet the minimum requirements. OpenGL 2.1 or higher is required. Please upgrade the driver of your video card.</source>
        <translation>Váš hardware nesplňuje minimální požadavky. Je vyžadováno OpenGL 2.1 a vyšší. Aktualizujte prosím ovladač grafické karty.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="436"/>
        <source>3DimViewer has detected that the previous run didn&apos;t finish correctly. Verify that you have the latest drivers for the video card.</source>
        <translation>3DimViewer zdetekoval, že předchozí relace neskončila správně. Ověřte, zda máte nejnovější ovladač grafické karty.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="450"/>
        <source>Your hardware doesn&apos;t meet the recommended configuration. You may experience reduced performance or the application won&apos;t run at all.</source>
        <translation>Váš hardware nesplňuje požadavky pro správný běh programu. Aplikace může běžet pomaleji nebo vůbec. </translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="451"/>
        <source>Don&apos;t show again</source>
        <translation>Příště nezobrazovat</translation>
    </message>
    <message>
        <source>Copyright %1 2008-2014 by 3Dim Laboratory s.r.o.</source>
        <translation type="obsolete">Copyright %1 2008-2014 3Dim Laboratory s.r.o.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="3515"/>
        <source>Copyright %1 2008-%2 by 3Dim Laboratory s.r.o.</source>
        <translation>Copyright %1 2008-%2 3Dim Laboratory s.r.o.</translation>
    </message>
</context>
<context>
    <name>SegmentationWidget</name>
    <message>
        <source>Tissue Segmentation</source>
        <translation type="vanished">Segmentace tkání</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="26"/>
        <source>Quick Tissue Model Creation</source>
        <translation>Rychlé vytvoření modelu tkáně</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="68"/>
        <source>Thresholding</source>
        <translation>Prahování</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="80"/>
        <location filename="../ui/segmentationwidget.ui" line="83"/>
        <location filename="../ui/segmentationwidget.ui" line="152"/>
        <location filename="../ui/segmentationwidget.ui" line="155"/>
        <source>Adjusts lower threshold.</source>
        <translation>Upraví dolní práh.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="102"/>
        <location filename="../ui/segmentationwidget.ui" line="105"/>
        <location filename="../ui/segmentationwidget.ui" line="130"/>
        <location filename="../ui/segmentationwidget.ui" line="133"/>
        <source>Adjusts higher threshold.</source>
        <translation>Upraví horní práh.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="179"/>
        <location filename="../ui/segmentationwidget.ui" line="182"/>
        <source>Applies coloring of data according to thresholds.</source>
        <translation>Podle prahů aplikuje barvení na data.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="185"/>
        <source>Apply Thresholds</source>
        <translation>Aplikovat prahování</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="192"/>
        <location filename="../ui/segmentationwidget.ui" line="195"/>
        <source>Sets color of thresholded data.</source>
        <translation>Nastaví barvu pro prahovaná data.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="216"/>
        <location filename="../ui/segmentationwidget.ui" line="219"/>
        <source>Picks lower threshold from data by clicking with left mose button.</source>
        <translation>Zvolte dolní práh z dat kliknutím levého tlačítka myši.</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="268"/>
        <source>Set to Active Region</source>
        <translation>Nastavit do aktivního regionu</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="278"/>
        <source>Model Creation</source>
        <translation>Vytvoření modelu</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="284"/>
        <source>Create Surface Model</source>
        <translation>Vytvořit model povrchu</translation>
    </message>
    <message>
        <source>Pick T1</source>
        <translation type="obsolete">Získat T1</translation>
    </message>
    <message>
        <location filename="../ui/segmentationwidget.ui" line="245"/>
        <location filename="../ui/segmentationwidget.ui" line="248"/>
        <source>Picks higher threshold from data by clicking with left mose button.</source>
        <translation>Zvolte horní práh z dat kliknutím levého tlačítka myši.</translation>
    </message>
    <message>
        <source>Pick T2</source>
        <translation type="obsolete">Získat T2</translation>
    </message>
</context>
<context>
    <name>VolumeRenderingWidget</name>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="20"/>
        <source>Volume Rendering</source>
        <translation>Volume Rendering</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="59"/>
        <source>Rendering Mode</source>
        <translation>Mód zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="67"/>
        <location filename="../ui/volumerenderingwidget.ui" line="70"/>
        <source>Selects MIP rendering mode.</source>
        <translation>Zvolí režim MIP.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="73"/>
        <source>MIP</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="80"/>
        <location filename="../ui/volumerenderingwidget.ui" line="83"/>
        <source>Selects Shaded rendering mode.</source>
        <translation>Zvolí režim Stínovaný.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="86"/>
        <source>Shaded</source>
        <translation>Stínovaný</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="93"/>
        <location filename="../ui/volumerenderingwidget.ui" line="96"/>
        <source>Selects X-Ray rendering mode.</source>
        <translation>Zvolí režim X-Ray.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="99"/>
        <source>X-Ray</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="106"/>
        <location filename="../ui/volumerenderingwidget.ui" line="109"/>
        <source>Selects Surface rendering mode.</source>
        <translation>Zvolí režim Povrchy.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="112"/>
        <source>Surface</source>
        <translation>Povrchy</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="119"/>
        <location filename="../ui/volumerenderingwidget.ui" line="122"/>
        <source>Selects rendering mode controlled by plugins.</source>
        <translation>Zvolí režim Vlastní.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="125"/>
        <source>Custom</source>
        <translation>Vlastní</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="137"/>
        <source>Coloring Lookup Tables</source>
        <translation>Mapování barev</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="143"/>
        <location filename="../ui/volumerenderingwidget.ui" line="146"/>
        <source>Selects look-up table.</source>
        <translation>Zvolí tabulku barev.</translation>
    </message>
    <message>
        <source>Bone surface</source>
        <translation type="vanished">Povrch kosti</translation>
    </message>
    <message>
        <source>Skin surface</source>
        <translation type="vanished">Měkké tkáně</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="150"/>
        <source>Bone Surface</source>
        <translation>Povrch kosti</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="155"/>
        <source>Skin Surface</source>
        <translation>Měkké tkáně</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="166"/>
        <source>Rendering Quality</source>
        <translation>Kvalita zobrazení</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="172"/>
        <location filename="../ui/volumerenderingwidget.ui" line="175"/>
        <source>Adjusts quality of volume rendering.</source>
        <translation>Nastaví kvalitu zobrazení.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="200"/>
        <source>Output Adjustment</source>
        <translation>Nastavení výstupu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="208"/>
        <source>Wind. Shift</source>
        <translation>Posun okénka</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="215"/>
        <location filename="../ui/volumerenderingwidget.ui" line="218"/>
        <source>Adjusts shift of density window.</source>
        <translation>Upraví posun denzitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="234"/>
        <source>Wind. Width</source>
        <translation>Šířka okénka</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="241"/>
        <location filename="../ui/volumerenderingwidget.ui" line="244"/>
        <source>Adjusts width of density window.</source>
        <translation>Upraví šířku denzitního okénka.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="260"/>
        <source>Brightness</source>
        <translation>Jas</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="267"/>
        <location filename="../ui/volumerenderingwidget.ui" line="270"/>
        <source>Adjusts brightness of volume rendering.</source>
        <translation>Upraví jas.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="286"/>
        <source>Contrast</source>
        <translation>Kontrast</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="293"/>
        <location filename="../ui/volumerenderingwidget.ui" line="296"/>
        <source>Adjusts contrast of volume rendering.</source>
        <translation>Upraví kontrast.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="312"/>
        <source>Surf. Tolerance</source>
        <translation>Tolerance povrchu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="319"/>
        <location filename="../ui/volumerenderingwidget.ui" line="322"/>
        <source>Adjusts surface tolerance.</source>
        <translation>Upraví toleranci povrchu.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="338"/>
        <source>Surf. Sharpness</source>
        <translation>Strmost povrchu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="345"/>
        <location filename="../ui/volumerenderingwidget.ui" line="348"/>
        <source>Adjusts surface sharpness.</source>
        <translation>Upraví strmost povrchu.</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="369"/>
        <source>Cutting Plane</source>
        <translation>Rovina řezu</translation>
    </message>
    <message>
        <location filename="../ui/volumerenderingwidget.ui" line="375"/>
        <location filename="../ui/volumerenderingwidget.ui" line="378"/>
        <source>Adjust cutting plane position.</source>
        <translation>Nastaví rovinu řezu.</translation>
    </message>
    <message>
        <source>Soft tissues</source>
        <translation type="obsolete">Měkké tkáně</translation>
    </message>
    <message>
        <source>Bone tissues</source>
        <translation type="obsolete">Kostní tkáň</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="obsolete">Průhledný</translation>
    </message>
    <message>
        <source>Air pockets</source>
        <translation type="obsolete">Vzduchové kapsy</translation>
    </message>
    <message>
        <source>Enhance soft</source>
        <translation type="obsolete">Zvýraznit měkké tkáně</translation>
    </message>
    <message>
        <source>Enhance hard</source>
        <translation type="obsolete">Zvýraznit kosti</translation>
    </message>
</context>
</TS>

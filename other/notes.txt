- Nastudovat a vytipovat mo�nosti vylep�en� vizu�ln� kvality renderingu pro lep�� anal�zu n�ro�n�ch dat (slo�it� struktury uvnit� - c�vy, materi�l typu houba, apod.)

     - jednodu��� v�ci: 2D LUT (intensity vs. gradient) pro barven�, ghosting, ...

     - slo�it�j��: rendering se st�ny... ambient occlusion...

- Prozkoumat existuj�c� SW (open source VR engine) - metoda Raycasting

     3DimViewer - https://bitbucket.org/3dimlab/3dimviewer/src/master/

     VTK -
https://www.vtk.org/doc/nightly/html/classvtkOpenGLGPUVolumeRayCastMapper.html

     http://www.inviwo.org/  (https://www.uni-muenster.de/Voreen/)

- Zkusit implementovat vylep�en� do st�vaj�c�ho SW - �pravy shader�, atd.

- Prov�st porovn�n� v�sledk� - vizu�ln� kvalita, frame rate



Ghosting / Bruckner - https://www.cg.tuwien.ac.at/research/publications/2005/bruckner-2005-ICV/bruckner-2005-ICV-Paper.pdf
!!occlusion?? KINDLMANN G., WHITAKER R., TASDIZEN
T., M�LLER T.: Curvature-based transfer functions for
direct volume rendering: Methods and applications. In
Proceedings of IEEE Visualization 2003 (2003), pp. 513�
520.

!!! 32. Kr�ger J, Westermann R: Acceleration techniques for GPU-based volume rendering. In: VIS�03: Proceedings of the 14th IEEE Visualization 2003 (VIS�03), IEEE Computer Society, Washington, DC, USA, 2003, pp. 287�292
!! 36. Gelder AV, Kim K: Direct volume rendering with shading via three-dimensional textures. In: Proc. of the 1996 Symposium on Volume Visualization. IEEE, 1996, pp 23�30
!! 27. Drebin R, Carpenter L, Hanrahan P: Volume rendering. In: Proceedings SIGGRAPH88, 1988, pp 65�74
28. Johnson C, Hansen C. Visualization Handbook. Orlando: Academic; 2004. 
29. Hadwiger M, Kniss JM, Rezk-salama C, Weiskopf D, Engel K. Real-Time Volume Graphics. 1. Natick: A. K. Peters; 2006. 
30. Levoy M. Display of surfaces from volume data. IEEE Comput Graph Appl. 1988;8(3):29�37.
31. H�hne KH, Bomans M, Pommert A, Riemer M, Schiers C, Tiede U, Wiebecke G. 3D visualization of tomographic volume data using the generalized voxel model. Vis Comput. 1990;6(1):28�36
32. Kr�ger J, Westermann R: Acceleration techniques for GPU-based volume rendering. In: VIS�03: Proceedings of the 14th IEEE Visualization 2003 (VIS�03), IEEE Computer Society, Washington, DC, USA, 2003, pp. 287�292
33. Westover L: Interactive volume rendering. In: Proceedings of the 1989 Chapel Hill Workshop on Volume visualization, ACM, 1989, pp 9�16
34. Westover L: Footprint evaluation for volume rendering. In: SIGGRAPH �90: Proceedings of the 17th annual conference on Computer graphics and interactive techniques, ACM Press, New York, NY, USA, 1990, pp. 367�376
35. Udupa JK, Odhner D. Shell rendering. IEEE Comput Graph Appl. 1993;13(6):58�67.
37. Marroquim R, Maximo A, Farias RC, Esperan�a C. Volume and isosurface rendering with GPU-accelerated cell projection. Comput Graph Forum. 2008;27(1):24�35.
38. Lacroute P, Levoy M. Fast volume rendering using a shear-warp factorization of the viewing transformation. Comput Graph. 1994;28(Annual Conference Series):451�458.
39. Mroz L, Hauser H, Gr�ller E. Interactive high-quality maximum intensity projection. Comput Graph Forum. 2000;19(3):341�350.
40. Robb R. X-ray computed tomography: from basic principles to applications. Annu Rev Biophys Bioeng. 1982;11:177�182.

49. Max N. Optical models for direct volume rendering. IEEE Trans Vis Comput Graph. 1995;1(2):99�108.

63. Kniss J, Kindlmann GL, Hansen CD. Multidimensional transfer functions for interactive volume rendering. IEEE Trans Vis Comput Graph. 2002;8(3):270�285.
69. Freiman M, Joskowicz L, Lischinski D, Sosna J. A feature-based transfer function for liver visualization. Int J CARS. 2007;2(1):125�126.
70. Pinto FM, Freitas CMDS. Volume visualization and exploration through flexible transfer function design. Comput Graph. 2008;32(5):540�549.

71. Zhang Q, Eaglesona R, Peters TM: Graphics hardware based volumetric medical dataset visualization and classification. In: Proc. of SPIE Med. Imaging, Vol. 6141, San Diego, CA, 2006, pp 61412T-1�11

!!!104. Ropinski T, Meyer-Spradow J, Diepenbrock S, Mensmann J, Hinrichs K. Interactive volume rendering with dynamic ambient occlusion and color bleeding. Comput Graph Forum. 2008;27(2):567�576.

!(toto je na prednasce pgr?) 110. Levoy M. Volume rendering by adaptive refinement. Vis Comput. 1990;6(1):2�7.

Z�kladn� rendering VR - https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3138940/
    MPR = multiplanar reformation
        -dopo�et dal��ch rovin ze sady 2D �ez�?
        -Curved MPR - zak�ivenou plochou je proveden �ez objemem, a density voxel� kter�mi proch�z� jsou n�sledn� rozbaleny do rovinn�ho zobrazen�
    DSR = direct surface rendering
        - speci�ln� p��pad volume renderingu
        - renderuje se povrch p��mo z objemu bez geometrick� interpretace
    DVR = direct volume rendering
        - zobrazen� objemu v projekci do 2D bez interpretace geometrick�ch vztah�
        --optical models
            paprsek proch�zej�c� mra�nem mal�ch ��stic        
    ISR = indirect surface rendering
        - surface modeling
    MIP = maximum intensity projection


direct volume rendering, vysv�tlen� optical models https://www.youtube.com/watch?v=hiaHlTLN9TE    




//toto funguje jako origin�l ///////////////////////////////////////////////////////////////////////////////////
                    // gradient magnitude 
                    float alphaDiv = length(normal) * maxNormalLength; 
 
                    if (alphaDiv > 0.005) 
                    { 
                        normal = normalize(normal); 


                        float C_a = 0.25; // ambient coef
                        float C_d = 0.75; // diffuse coef

                        float fshade = C_d * saturate(dot(normal, vLight))  + C_a; //zatim bez specular                         
                        vec3 color_p = fshade * LUT.rgb;
                      
                        float alpha_p = saturate(1.0 - LUT.a)  * textureSampling;

                        //accumulated values
                        color = color + color_p * alpha_p * saturate(1.0 - alpha);
                        alpha = alpha + alpha_p * saturate(1.0 - alpha);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 


//modulace magnitudou ///////////////////////////////////////////////////////////////////////////////////
                  // gradient magnitude 
                    float alphaDiv = length(normal) * maxNormalLength; 

                    
 
                    if (alphaDiv > 0.005) 
                    { 
                        alphaDiv = length(normal);
                        alphaDiv *= 3.0;

                        normal = normalize(normal); 


                        float C_a = 0.25; // ambient coef
                        float C_d = 0.75; // diffuse coef

                        float fshade = C_d * saturate(dot(normal, vLight))  + C_a; //zatim bez specular                         
                        vec3 color_p = fshade * LUT.rgb;
                      
                         
                         
                        
                        float alpha_p = saturate(1.0 - LUT.a)*alphaDiv  * textureSampling;

                        //accumulated values
                        color = color + color_p * alpha_p * saturate(1.0 - alpha);
                        alpha = alpha + alpha_p * saturate(1.0 - alpha);
                        



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 

//Blinn-Phong ///////////////////////////////////////////////////////////////////////////////////
                    float alphaDiv = length(normal) * maxNormalLength; 

                    
 
                    if (alphaDiv > 0.005) 
                    { 
                        alphaDiv = length(normal);
                        alphaDiv *= 3.0;

                        normal = normalize(normal); 

                        ///////////////////////////PGR
                        float C_e = 1250.0; // specular exponent                         
                        float C_s = 0.5;  // specular coef                      
                        vec3 H = normalize(vLight + vCamera); 

                        float C_a = 0.25; // ambient coef
                        float C_d = 0.75; // diffuse coef

                        float fshade = C_d * saturate(dot(normal, vLight)) +  C_s*pow(saturate(dot(H, normal)),C_e)  + C_a; //zatim bez specular                         
                        vec3 color_p = fshade * LUT.rgb;
                      
                         
                         
                        
                        //float alpha_p = saturate(1.0 - LUT.a)*alphaDiv  * textureSampling;
                        float alpha_p = saturate(1.0 - LUT.a) * textureSampling;

                        //accumulated values
                        color = color + color_p * alpha_p * saturate(1.0 - alpha);
                        alpha = alpha + alpha_p * saturate(1.0 - alpha);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
================================================================================
- Prebuilt 3rd party libraries
-
- Copyright (c) 2008-2017 by 3Dim Laboratory s.r.o.
================================================================================

This is a package of prebuilt 3rd party libraries for use with the 
software produced by 3Dim Laboratory company.

- Unpack content of this archive into an arbitrary directory.
  
- Set the TRIDIM_3RDPARTY_DIR variable when running the CMake utility.

================================================================================

var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz~",
  1: "abcdefghijlmnopqrstuvwxy",
  2: "_abcdefghijklmnopqrstuvwxz~",
  3: "abcdefghijklmnopqrstuvwxyz",
  4: "cfistvx",
  5: "eilx",
  6: "acdefghimoprtvw",
  7: "dgost",
  8: "cdhnpstu",
  9: "acdefhilmortw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "functions",
  3: "variables",
  4: "typedefs",
  5: "enums",
  6: "enumvalues",
  7: "related",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Functions",
  3: "Variables",
  4: "Typedefs",
  5: "Enumerations",
  6: "Enumerator",
  7: "Friends",
  8: "Modules",
  9: "Pages"
};


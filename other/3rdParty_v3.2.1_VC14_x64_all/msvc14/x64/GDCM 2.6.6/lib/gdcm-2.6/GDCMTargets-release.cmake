#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "gdcmCommon" for configuration "Release"
set_property(TARGET gdcmCommon APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmCommon PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "ws2_32;crypt32"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmCommon.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmCommon )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmCommon "${_IMPORT_PREFIX}/lib/gdcmCommon.lib" )

# Import target "gdcmDICT" for configuration "Release"
set_property(TARGET gdcmDICT APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmDICT PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "gdcmDSED;gdcmIOD"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmDICT.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmDICT )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmDICT "${_IMPORT_PREFIX}/lib/gdcmDICT.lib" )

# Import target "gdcmDSED" for configuration "Release"
set_property(TARGET gdcmDSED APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmDSED PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "gdcmCommon;gdcmzlib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmDSED.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmDSED )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmDSED "${_IMPORT_PREFIX}/lib/gdcmDSED.lib" )

# Import target "gdcmIOD" for configuration "Release"
set_property(TARGET gdcmIOD APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmIOD PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "gdcmDSED;gdcmCommon;gdcmexpat"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmIOD.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmIOD )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmIOD "${_IMPORT_PREFIX}/lib/gdcmIOD.lib" )

# Import target "gdcmMSFF" for configuration "Release"
set_property(TARGET gdcmMSFF APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmMSFF PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "gdcmIOD;gdcmDSED;gdcmDICT;gdcmjpeg8;gdcmjpeg12;gdcmjpeg16;gdcmopenjpeg;gdcmcharls;rpcrt4"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmMSFF.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmMSFF )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmMSFF "${_IMPORT_PREFIX}/lib/gdcmMSFF.lib" )

# Import target "gdcmMEXD" for configuration "Release"
set_property(TARGET gdcmMEXD APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmMEXD PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "gdcmMSFF;gdcmDICT;gdcmDSED;gdcmIOD;socketxx;ws2_32"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmMEXD.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmMEXD )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmMEXD "${_IMPORT_PREFIX}/lib/gdcmMEXD.lib" )

# Import target "gdcmjpeg8" for configuration "Release"
set_property(TARGET gdcmjpeg8 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmjpeg8 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmjpeg8.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmjpeg8 )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmjpeg8 "${_IMPORT_PREFIX}/lib/gdcmjpeg8.lib" )

# Import target "gdcmjpeg12" for configuration "Release"
set_property(TARGET gdcmjpeg12 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmjpeg12 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmjpeg12.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmjpeg12 )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmjpeg12 "${_IMPORT_PREFIX}/lib/gdcmjpeg12.lib" )

# Import target "gdcmjpeg16" for configuration "Release"
set_property(TARGET gdcmjpeg16 APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmjpeg16 PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmjpeg16.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmjpeg16 )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmjpeg16 "${_IMPORT_PREFIX}/lib/gdcmjpeg16.lib" )

# Import target "gdcmexpat" for configuration "Release"
set_property(TARGET gdcmexpat APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmexpat PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmexpat.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmexpat )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmexpat "${_IMPORT_PREFIX}/lib/gdcmexpat.lib" )

# Import target "gdcmopenjpeg" for configuration "Release"
set_property(TARGET gdcmopenjpeg APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmopenjpeg PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmopenjpeg.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmopenjpeg )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmopenjpeg "${_IMPORT_PREFIX}/lib/gdcmopenjpeg.lib" )

# Import target "gdcmcharls" for configuration "Release"
set_property(TARGET gdcmcharls APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmcharls PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmcharls.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmcharls )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmcharls "${_IMPORT_PREFIX}/lib/gdcmcharls.lib" )

# Import target "gdcmzlib" for configuration "Release"
set_property(TARGET gdcmzlib APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmzlib PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmzlib.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmzlib )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmzlib "${_IMPORT_PREFIX}/lib/gdcmzlib.lib" )

# Import target "gdcmgetopt" for configuration "Release"
set_property(TARGET gdcmgetopt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(gdcmgetopt PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/gdcmgetopt.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS gdcmgetopt )
list(APPEND _IMPORT_CHECK_FILES_FOR_gdcmgetopt "${_IMPORT_PREFIX}/lib/gdcmgetopt.lib" )

# Import target "socketxx" for configuration "Release"
set_property(TARGET socketxx APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(socketxx PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LINK_INTERFACE_LIBRARIES_RELEASE "ws2_32.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/socketxx.lib"
  )

list(APPEND _IMPORT_CHECK_TARGETS socketxx )
list(APPEND _IMPORT_CHECK_FILES_FOR_socketxx "${_IMPORT_PREFIX}/lib/socketxx.lib" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)

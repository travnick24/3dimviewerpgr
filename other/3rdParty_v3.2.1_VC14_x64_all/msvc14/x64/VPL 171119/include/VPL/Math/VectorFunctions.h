//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 * 
 * Medical Data Segmentation Toolkit (MDSTk) 
 * Copyright (c) 2003-2006 by Michal Spanel  
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/02/15                       
 *
 * Description:
 * - Basic vector functions and operations.
 */

#ifndef VPL_VECTORFUNCTIONS_H
#define VPL_VECTORFUNCTIONS_H


//==============================================================================
/*
 * Include all predefined vector functions.
 */

// General vector functions
#include "VectorFunctions/General.h"

// Conversion of fundamental numbers to vectors
#include "VectorFunctions/Conversion.h"


#endif // VPL_VECTORFUNCTIONS_H


//==============================================================================
/* This file comes from MDSTk software and was modified for
 * 
 * VPL - Voxel Processing Library
 * Changes are Copyright 2014 3Dim Laboratory s.r.o.
 * All rights reserved.
 * 
 * Use of this source code is governed by a BSD-style license that can be
 * found in the LICENSE file.
 * 
 * The original MDSTk legal notice can be found below.
 *
 * Medical Data Segmentation Toolkit (MDSTk)    \n
 * Copyright (c) 2003-2006 by Michal Spanel     \n
 *
 * Author:  Michal Spanel, spanel@fit.vutbr.cz  \n
 * Date:    2006/02/15                          \n
 *
 * Description:
 * - Basic matrix functions and operations.
 */

#ifndef VPL_MATRIXFUNCTIONS_H
#define VPL_MATRIXFUNCTIONS_H


//==============================================================================
/*
 * Include all predefined matrix functions.
 */

// General matrix functions
#include "MatrixFunctions/General.h"

// Special matrix funtions
#include "MatrixFunctions/Determinant.h"
#include "MatrixFunctions/Inverse.h"
#include "MatrixFunctions/Eig.h"


#endif // VPL_MATRIXFUNCTIONS_H


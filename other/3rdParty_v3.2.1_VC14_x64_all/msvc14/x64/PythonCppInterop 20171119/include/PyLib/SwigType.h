//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#ifndef VPLSWIG_SWIGTYPE_H
#define VPLSWIG_SWIGTYPE_H

#include <string>
#include <vector>
#include "Base.h"


namespace pylib
{
class CSwigType
{
public:
	CSwigType(std::string className);
    CSwigType(std::string className, std::string type1);
    CSwigType(std::string className, std::string type1, std::string type2);

    template <class T>
    inline T pyObject2Class(PyObject* object)
    {
        void *argp1 = 0;
        SwigTypePointer swigTypePointer;
        swig_type_info* swigType = swigTypePointer.GetSwigType(CreateTypeString());
        if (swigType != nullptr)
        {
            const int res = SWIG_ConvertPtr(object, &argp1, swigType, 0);

            if (SWIG_IsOK(res))
            {
                return reinterpret_cast<T>(argp1);
            }
        }
        return NULL;
    };

    template <class T>
    inline PyObject* class2PyObject(T object)
    {
        SwigTypePointer swigTypePointer;
        swig_type_info* swigType = swigTypePointer.GetSwigType(CreateTypeString());
        if (swigType != nullptr)
        {
            return SWIG_NewPointerObj((void*)object, swigType, 0 | 0);
        }
        return nullptr;
    };

    std::string CreateTypeString();
    

    
private:
    std::string m_className;
    std::vector<std::string> m_types;
    
};
}

#endif // VPLSWIG_SWIGTYPE_H
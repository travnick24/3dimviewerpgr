///////////////////////////////////////////////////////////////////////////////
// $Id$
//
// 3DimViewer
// Lightweight 3D DICOM viewer.
//
// Copyright 2008-2016 3Dim Laboratory s.r.o.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
///////////////////////////////////////////////////////////////////////////////

#ifndef CPythonInterpret_H
#define CPythonInterpret_H

/////////////////////////////////////////////////////////
// Includes
#include <string>
#include "PythonEnv.h"
// forward declaration
class CPythonInterpret;

namespace pylib
{

////////////////////////////////////////////////////////////
//! Initilize python interpret. 
class CPythonInterpret
{

public:
    CPythonInterpret() {}

    void Close();

    ~CPythonInterpret() {}
    bool initilizeInterpret(std::string pythonPath = "");
    void initilizeInterpretWithVPLSwig(std::string vplSwigDir, std::string vplSwigName, std::string pythonPath = "");
    bool loadModule(std::string path, std::string name);
    CPyErrorMessage getError();
    CPython* get();

private:
    static CPython m_python;
    static bool IsOpenInterpret;
};
}
#endif // CPythonInterpret_H


////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////

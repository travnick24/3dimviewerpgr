//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/
#ifndef VPLSWIG_MODULEINFO_H
#define VPLSWIG_MODULEINFO_H

#include <string>
#include <vector>

namespace pylib
{

class CFunctionInfo
{
public:
    CFunctionInfo();
    ~CFunctionInfo();
    std::string description;
    std::string declaration;
};


class CModuleInfo
{
public:
    CModuleInfo(std::string filePath, std::string infoTag = "INFO:");
    ~CModuleInfo();

    std::string description;
    std::vector<CFunctionInfo> functions;
private:
    void parseFile();
    std::string filePath;
    std::string infoTag;
};
}

#endif // VPLSWIG_MODULEINFO_H
//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#ifndef VPLSWIG_PYERRORMESSAGE_H
#define VPLSWIG_PYERRORMESSAGE_H

namespace pylib
{
/**
* Manages the Python error messages.
*/
class CPyErrorMessage
{

public:

	CPyErrorMessage();
	~CPyErrorMessage();

	void Clear();

	std::string header;
	std::string type;
	std::string body;
	std::string traceback;

	std::string getMessage();

}; // class CPyErrorMessage

}
#endif // VPLSWIG_PYERRORMESSAGE_H


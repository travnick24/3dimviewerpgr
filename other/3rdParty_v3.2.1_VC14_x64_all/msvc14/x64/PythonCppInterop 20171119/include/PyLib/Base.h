//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#ifndef VPLSWIG_BASE_H
#define VPLSWIG_BASE_H


#include <string>
#include <map>
#include <vector>
#include <exception>
#include "runtime.h"

#define STR_EXPAND(tok) #tok
#define DEFINE2STRING(tok) STR_EXPAND(tok)


namespace pylib
{
/**
* Manages the Python environment.
* All python api calls must be wrapped in
* the context of a CPythonBase object.
*/



class CPythonBase
{

public:

    CPythonBase(int argc, char** argv, std::string pythonInterpretPath = "");
    CPythonBase(std::string pythonInterpretPath = "");
    ~CPythonBase();
private:
    void Initilize();
    std::string pythonInterpretPath;
    
}; // class CPythonBase



   /**
   * Exception thrown by Python api calls.
   */
class CPythonException : public std::exception
{

public:

    CPythonException(const std::string& m);
    virtual ~CPythonException() throw();

    virtual const char* what() throw();

private:

    std::string message;

}; // class CPythonException

class SwigTypePointer
{
public:

    swig_type_info* GetSwigType(std::string type);
private:    
    swig_type_info* m_swigType = nullptr;

};


typedef std::map<std::string, std::string> tStringMap;
typedef std::vector<std::string> tStringList;



}
#endif // VPLSWIG_BASE_H


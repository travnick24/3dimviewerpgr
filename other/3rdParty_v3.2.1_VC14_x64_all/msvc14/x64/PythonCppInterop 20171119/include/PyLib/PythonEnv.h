//==============================================================================
/* This file is part of
*
* VPLSWIG - Voxel Processing Library
* Copyright 2017 3Dim Laboratory s.r.o.
* All rights reserved.
*
* Use of this source code is governed by a BSD-style license that can be
* found in the LICENSE file.
*/

#ifndef VPLSWIG_PYTHONINTERPRET_H
#define VPLSWIG_PYTHONINTERPRET_H

#include <string>
#include <map>
#include <vector>
#include <exception>
#include "SwigType.h"
#include "Base.h"
#include "PyErrorMessage.h"


namespace pylib
{
//==============================================================================
/*!
* This class provide wrappers to low-level python calls.
*/
class CPython
{

public:

    CPython() {}

	//! Initialize python interpret with arguments.
    void Init(int argc, char** argv, std::string pythonInterpretPath = "");

	//! Initialize python.
    void Init(std::string pythonInterpretPath = "");

	//! Close python terminal and clear loaded modules.
	~CPython();

    void close();

    

	CPyErrorMessage GetError();

    void addArgument(PyObject* argument);

    void addArgument(std::string argument);

    void addArgument(long argument);
    void addArgument(double argument);

	void SaveError();

	//! Run simple string in python interpret.
    bool runString(const std::string& s);


 
	bool runFile(const std::string& f);

    bool loadModules(tStringList modules);

	bool load(const std::string& moduleName);


 
	bool call(const std::string& moduleName, const std::string& funcName);

   
    bool call(const std::string& moduleName, const std::string& funcName,long& ret);


	bool call(const std::string& moduleName, const std::string& funcName,PyObject** ret);

    /**
    * Executes a function in the module.
    * The function should return a real number
    * and this is copied to ret.
    * @param funcName
    * @param args This should be empty if the function
    * takes no arguments.
    * @param ret Functions return value, if any is copied
    * here.
    */
	bool call(const std::string& moduleName, const std::string& funcName,double& ret);


    /**
    * Executes a function in the module.
    * Any return values, despite type, is copied to
    * ret.
    * @param funcName
    * @param args This should be empty if the function
    * takes no arguments.
    * @param ret Functions return value, if any is copied
    * here.
    */
	bool call(const std::string& moduleName, const std::string& funcName,std::string& ret);

	void addModulePath(std::string path);


    /**
    * Executes a function in the module.
    * The function should return a list or a tuple,
    * and this is copied to ret.
    * @param funcName
    * @param args This should be empty if the function
    * takes no arguments.
    * @param ret Functions return value, if any is copied
    * here.
    */
	bool call(const std::string& moduleName, const std::string& funcName,tStringList& ret);


    /**
    * Executes a function in the module.
    * The function should return a dictionary,
    * and this is copied to ret.
    * @param funcName
    * @param args This should be empty if the function
    * takes no arguments.
    * @param ret Functions return value, if any is copied
    * here.
    */
	bool call(const std::string& moduleName, const std::string& funcName,tStringMap& ret);


private:

    /**
    * Retrieves a function handle from the python module.
    * @param funcName
    * @return PyObject*
    */
	bool getFunctionObject(const std::string moduleName, const std::string& funcName, PyObject** retFunction);


    /**
    * Converts an tArgumentMap to a tuple of PyObjects
    * so that they can be passed as arguments to a
    * python function.
    * @param args tArgumentMap to convert
    * @return PyObject* args converted to a python tuple.
    */
												
	PyObject* createArgsTuple();

    /**
    * Makes the actual function call, with the help of the
    * above functions.
    * @param funcName
    * @param args
    * @return PyObject* the value returned by the python function,
    * or null if no value is returned.
    */
	bool makeCall(const std::string& moduleName, const std::string& funcName,PyObject** retObject);


private:

    CPythonBase* env;
    std::map <std::string, PyObject*> modules;


	std::vector<PyObject*> m_arguments;
	CPyErrorMessage m_Error;

}; // class CPython

}
#endif // VPLSWIG_PYTHONINTERPRET_H

